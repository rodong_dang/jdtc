package com.rodong.jdtc.member.controller;

import java.util.List;

import com.rodong.jdtc.member.model.dto.MemberDTO;
import com.rodong.jdtc.member.model.service.AdminService;

public class AdminController {
	
	private AdminService adminService;
	private List<MemberDTO> selectAllMemberList;
	
	/* 전체 리스트 조회*/
	public List<MemberDTO> selectAllMember() {
		
		adminService = new AdminService();
		selectAllMemberList = adminService.selectAllMember();
		
		return selectAllMemberList;
	}
	
}
