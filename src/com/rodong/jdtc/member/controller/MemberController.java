package com.rodong.jdtc.member.controller;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import com.rodong.jdtc.member.model.dto.MemberDTO;
import com.rodong.jdtc.member.model.service.MemberService;

/* Member(Account)에 대한 Controller */
public class MemberController {
	
	private final MemberService memberService;
	
	/* MemberService를 호출하기 위한 기본 생성자 */
	public MemberController() {
		this.memberService = new MemberService();
	}
	
	/* View에서 받아온 String들을 Key, Value 값으로 이용 위해 Map 사용 */
	public boolean signUpMember(Map<String, String> requestMap) {
		
		/* 회원가입 */
		/* 회원가입 성공 여부 확인용 boolean 변수 선언 */
		boolean isSignUp = false;
		
		String id = requestMap.get("ID");
		String pwd = requestMap.get("PWD");
		String name = requestMap.get("NAME");
		String email = requestMap.get("EMAIL");
		
		/* 회원가입 시 가입일자 추가로 input */
		java.util.Date userSignUpDate = new java.util.Date(System.currentTimeMillis());
		SimpleDateFormat dateFormat = new SimpleDateFormat("yy/MM/dd");
		String signUpDate = dateFormat.format(dateFormat);
		
		MemberDTO member = new MemberDTO();
		
		member.setId(id);
		member.setPwd(pwd);
		member.setName(name);
		member.setEmail(email);
		member.setSignUpDate(signUpDate);
		
		/* 회원가입이 성공적으로 됐는지 check */
		int result = memberService.signUpUser(member);
		
		if(result > 0) {
			isSignUp = true;
		}
		
		return isSignUp;
	}
	
	public boolean memberLogin(String id, String pwd) {
		
		/* 로그인 */
		/* 입력받은 값과 비교 위한 id, pwd List (String type) */
		List<String> idList = memberService.selectId();
		List<String> pwdList = memberService.selectPwd();
		
		/* 로그인 성공 여부 확인용 boolean 변수 선언 */
		boolean isLogin = false;
		
		/*  */
			if(idList.contains(id) && pwdList.contains(pwd)) {
				/* 로그인 성공 */
				isLogin = true;
			}
		
		return isLogin;
	}
	
	public String findAccount(String name, String email) {
		/* 아이디 찾기*/
		String findId = memberService.findUserId(name, email);
		
		return findId;
		
	}
	
	public String findAccount(String name, String id, String email) {				
		/* 비밀번호 찾기 (오버로딩) */
		String findPwd = memberService.findUserPwd(name, id, email);
		
		return findPwd;
	}
	
	public boolean withDrawalAccount(String id, String pwd) {
		/* 계정 탈퇴 */
		
		boolean withDrawalresult = memberService.userWithdrawal(id, pwd);
		
		return withDrawalresult;
		
	}
}
