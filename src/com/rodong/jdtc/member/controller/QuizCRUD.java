package com.rodong.jdtc.member.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.rodong.jdtc.member.model.dto.QuizDTO;
import com.rodong.jdtc.member.model.service.QuizService;

public class QuizCRUD {

	private final QuizController quizController;

	Scanner sc = new Scanner(System.in);

	public QuizCRUD() {
		this.quizController = new QuizController();
	}

	QuizDTO quizDTO = new QuizDTO();

	/* Select */
	public void displayQuizMenu() {

		QuizController quizController = new QuizController();

		do {
			int select;

			System.out.println("1. 퀴즈조회");
			System.out.println("2. 퀴즈생성");
			select = sc.nextInt();

			switch (select) {

			case 1:
				quizController.selectAllQuiz();
				break;
			case 2:
				quizController.insertQuiz(inputQuiz());
				break;
			}
		} while (true);
	}

	/* 퀴즈 생성하기 */
	public Map<String, String> inputQuiz() {
		
		Map<String, String> quiz = new HashMap<>();
		sc.nextLine();
		System.out.println("퀴즈 내용 : ");
		quiz.put("quizContent", sc.nextLine());
		System.out.print("퀴즈 정답 번호 : ");
		quiz.put("quizAnswerNo", sc.nextLine());
		System.out.print("퀴즈 정답 내용 : ");
		quiz.put("quizAnswer", sc.nextLine());
		System.out.print("퀴즈 타입 : ");
		quiz.put("quizType", sc.nextLine());
		System.out.print("퀴즈 난이도번호 : ");
		quiz.put("DifficultyCode", sc.nextLine());
		System.out.print("퀴즈 난이도명 : ");
		quiz.put("DifficultyName", sc.nextLine());

		return quiz;

	}
	
	/* Quiz_NO를 조회해서 퀴즈 내용 수정하기 */
	

}