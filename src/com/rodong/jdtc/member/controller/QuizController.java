package com.rodong.jdtc.member.controller;

import java.util.List;
import java.util.Map;

import com.rodong.jdtc.member.model.dto.QuizDTO;
import com.rodong.jdtc.member.model.service.QuizService;

/* Quiz에 대한 Controller */
public class QuizController {

	private final QuizService quizService;
	
	public QuizController() {
		this.quizService = new QuizService();
	}

	/* Select All Quiz */
	public List<QuizDTO> selectAllQuiz() {
		return quizService.selectAllQuiz();
	}
	
	/* 퀴즈 생성하기 (hashMap으로 받은 값을 DTO에 담기) */
	public void insertQuiz(Map<String, String> quiz) {
		QuizDTO quizDTO = new QuizDTO();
		quizDTO.setQuizContent(quiz.get("quizContent"));
		quizDTO.setQuizAnswerNo(Integer.valueOf(quiz.get("quizAnswerNo")));
		quizDTO.setQuizAnswer(quiz.get("quizAnswer"));
		quizDTO.setQuizType(quiz.get("quizType"));
		quizDTO.setQuizDifficultyCode(Integer.valueOf(quiz.get("DifficultyCode")));
		quizDTO.setQuizDifficultyName(quiz.get("DifficultyName"));
		
		quizService.insertQuiz(quizDTO);
	}
	
	/* 퀴즈번호로 퀴즈내용 조회*/
	public QuizDTO selectContentByQuizNo(int quizNo) {
		QuizDTO quiz = quizService.selectByQuizNo(quizNo);
		return quiz;
	}
	
	/* 난이도명으로 퀴즈번조회 */
	public List<QuizDTO> selectQuizNoByDifficultName(String DifficultName) {
		List<QuizDTO> quizList = quizService.selectQuizNoByDifficultName(DifficultName);
		return quizList;
	}
	
	/* 퀴즈넘버로 정답번호 정답내용 조회*/
	public List<QuizDTO> selectAnswerContentByQuizNo(int quizNo) {
		List<QuizDTO> quizList = quizService.selectAnswerContentByQuizNo(quizNo);
		return quizList;
	}

	/* 퀴즈 번호로 퀴즈 정답번호 조회*/
	public QuizDTO selectCompareAnswer(int quizNo) {
		QuizDTO quiz = quizService.selectCompareAnswer(quizNo);
		return quiz;
	}
	
}
