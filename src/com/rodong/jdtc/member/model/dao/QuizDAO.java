package com.rodong.jdtc.member.model.dao;

import static com.rodong.common.JDBCTemplate.close;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.rodong.jdtc.member.model.dto.QuizDTO;

/* 퀴즈에 대한 DAO */
public class QuizDAO {

	private Properties prop;

	public QuizDAO() {

		prop = new Properties();

		try {
			prop.loadFromXML(new FileInputStream("mapper/query.xml"));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/* 퀴즈 전부 조회 */
	public List<QuizDTO> selectAllQuiz(Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		List<QuizDTO> quizList = null;

		String query = prop.getProperty("selectAllQuiz");

		try {
			pstmt = con.prepareStatement(query);
			rset = pstmt.executeQuery();

			quizList = new ArrayList<>();

			while (rset.next()) {
				QuizDTO quiz = new QuizDTO();
				quiz.setQuizNo(rset.getInt("QUIZ_NO"));
				quiz.setQuizContent(rset.getString("QUIZ_CONTENT"));
//				quiz.setQuizAnswerNo(rset.getInt("QUIZ_ANSWER_NO"));
//				quiz.setQuizAnswer(rset.getString("QUIZ_ANSWER"));
//				quiz.setQuizType(rset.getString("QUIZ_TYPE"));
//				quiz.setQuizDifficultyCode(rset.getInt("DIFFICULTY_CODE"));
//				quiz.setQuizDifficultyName(rset.getString("DIFFICULTY_NAME"));

				quizList.add(quiz);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		return quizList;

	}

	/* 퀴즈생성 */
	public int insertQuiz(Connection con, QuizDTO quiz) {
		PreparedStatement pstmt = null;
		int result = 0;
		String query = prop.getProperty("insertQuiz");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, quiz.getQuizContent());
			pstmt.setInt(2, quiz.getQuizAnswerNo());
			pstmt.setString(3, quiz.getQuizAnswer());
			pstmt.setString(4, quiz.getQuizType());
			pstmt.setInt(5, quiz.getQuizDifficultyCode());
			pstmt.setString(6, quiz.getQuizDifficultyName());

			result = (Integer) (pstmt.executeUpdate());

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		return result;

	}

	/* 퀴즈번호로 퀴즈내용 조회하기 */
	public QuizDTO selectByQuizNo(Connection con, int quizNo) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;

		QuizDTO quiz = null;

		String query = prop.getProperty("selectByQuizNo");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, quizNo);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				quiz = new QuizDTO();
				quiz.setQuizContent(rset.getString("QUIZ_CONTENT"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		return quiz;
	}

	/* 난이도명으로 퀴즈번호 조회 */
	public List<QuizDTO> selectQuizNoByDifficultName(Connection con, String difficultName) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		List<QuizDTO> quizList = null;

		String query = prop.getProperty("selectQuizNoByDifficultName");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, difficultName);

			rset = pstmt.executeQuery();

			quizList = new ArrayList<>();

			while (rset.next()) {
				QuizDTO quiz = new QuizDTO();
				quiz.setQuizNo(rset.getInt("QUIZ_NO"));

				quizList.add(quiz);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}

		return quizList;
	}

	/* 퀴즈번호로 정답 번호, 내용 조회 */
	public List<QuizDTO> selectAnswerContentByQuizNo(Connection con, int quizNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		List<QuizDTO> quizList = null;

		String query = prop.getProperty("selectAnswerByQuizNo");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, quizNo);

			rset = pstmt.executeQuery();

			quizList = new ArrayList<>();

			while (rset.next()) {
				QuizDTO quiz = new QuizDTO();
				quiz.setContent_No(rset.getInt("CONTENT_NO"));
				quiz.setContent_View(rset.getString("CONTENT_VIEW"));

				quizList.add(quiz);
				System.out.println(quizList);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}

		return quizList;
	}

	/* 퀴즈번호로 정답 번호, 내용 조회 */
	public QuizDTO selectCompareAnswer(Connection con, int quizNo) {

		PreparedStatement pstmt = null;
		
		ResultSet rset = null;

		QuizDTO quiz = null;

		String query = prop.getProperty("selectCompareAnswer");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, quizNo);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				quiz = new QuizDTO();
				quiz.setQuizAnswerNo(rset.getInt("QUIZ_ANSWER_NO"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		return quiz;
	}
}
