package com.rodong.jdtc.member.model.dao;

<<<<<<< src/com/rodong/jdtc/member/model/dao/MemberDAO.java
import static com.rodong.common.JDBCTemplate.close;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
=======
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
>>>>>>> src/com/rodong/jdtc/member/model/dao/MemberDAO.java
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
<<<<<<< src/com/rodong/jdtc/member/model/dao/MemberDAO.java
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import com.rodong.jdtc.member.model.dto.MemberDTO;
import com.rodong.jdtc.member.view.menu.Member_List;

//DB 처리
public class MemberDAO {
	
	
	private static final String user  = " C##RODONG_TEST"; //DB ID
	private static final String  password = "RODONG"; //DB 패스워드

	private Properties prop;
	
		

	Member_List mList;
=======

import static com.rodong.common.JDBCTemplate.close;

import com.rodong.jdtc.member.model.dto.MemberDTO;

/* Member(Account)에 대한 DAO */
public class MemberDAO {

	private Properties prop;
>>>>>>> src/com/rodong/jdtc/member/model/dao/MemberDAO.java

	public MemberDAO() {

		prop = new Properties();
		try {
			prop.loadFromXML(new FileInputStream("mapper/query.xml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
<<<<<<< src/com/rodong/jdtc/member/model/dao/MemberDAO.java
	}


	public MemberDAO(Member_List mList){
		this.mList = mList;
		System.out.println("DAO=>"+mList);
	}


 /* 모든 정보 조회용 메소드 */
public List<MemberDTO> selectAllMember(Connection con) {
	PreparedStatement ps = null;
	ResultSet rs = null;
	List<MemberDTO> selectAllMemberList = new ArrayList<>();
	String query = prop.getProperty("SelectAdminUser");
	
	try {
		ps = con.prepareStatement(query);
		rs = ps.executeQuery();
		
	
		while(rs.next()){
			MemberDTO dto = new MemberDTO();
			dto.setNo(rs.getInt("USER_NO"));
			dto.setName(rs.getString("USER_NAME"));
			dto.setEmail(rs.getString("USER_EMAIL"));
			dto.setId(rs.getString("USER_ID"));
			dto.setPwd(rs.getString("USER_PWD"));
			dto.setType(rs.getString("USER_TYPE"));
			dto.setLife(rs.getInt("LIFE"));
			dto.setSaveDate(rs.getDate("SAVE_DATE"));
			dto.setFloor(rs.getInt("FLOOR_NO"));
			dto.setSaveNo(rs.getInt("SAVE_NO"));
			dto.setNy(rs.getString("USER_WITHDRAWAL_EN"));
			dto.setSignupDate(rs.getDate("USER_SIGNUP_DATE"));
			
			selectAllMemberList.add(dto);
		}
	} catch (SQLException e) {
		e.printStackTrace();
	}finally {
		close(ps);
		close(rs);
	}
	return selectAllMemberList;
}

	
	/**한사람의 회원 정보를 얻는 메소드*/
	public MemberDTO getMemberDTO(Connection con, String id){

		MemberDTO dto = new MemberDTO();

		PreparedStatement ps = null; //명령
		ResultSet rs = null;         //결과

		String query = prop.getProperty("SelectAdminUser");
		
		try {
			
			ps = con.prepareStatement(query);
			ps.setString(1, id);

			rs = ps.executeQuery();

			if(rs.next()){
				dto.setNo(rs.getInt("USER_NO"));
				dto.setName(rs.getString("USER_NAME"));
				dto.setEmail(rs.getString("USER_EMAIL"));
				dto.setId(rs.getString("USER_ID"));
				dto.setPwd(rs.getString("USER_PWD"));
				dto.setType(rs.getString("USER_TYPE"));
				dto.setLife(rs.getInt("LIFE"));
				dto.setSaveDate(rs.getDate("SAVE_DATE"));
				dto.setFloor(rs.getInt("FLOOR_NO"));
				dto.setSaveNo(rs.getInt("SAVE_NO"));
				dto.setNy(rs.getString("USER_WITHDRAWAL_EN"));
				dto.setSignupDate(rs.getDate("USER_SIGNUP_DATE"));

			}
		} catch (Exception e) {
			e.printStackTrace();
		}  finally {
			close(ps);
			close(rs);
		}

		return dto;    
	}


=======

	}
	
	public int SignUpUser(Connection con, MemberDTO member) {

		PreparedStatement pstmt = null;
		int result = 0;

		String query = prop.getProperty("SignUpUser");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, member.getName());
			pstmt.setString(2, member.getPwd());
			pstmt.setString(3, member.getId());
			pstmt.setString(4, member.getPwd());
			pstmt.setString(5, member.getSignUpDate());

			result = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		return result;

	}


	public List<MemberDTO> selectAllAccount(Connection con) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;

		List<MemberDTO> memberList = null;

		String query = prop.getProperty("selectUser");

		try {
			pstmt = con.prepareStatement(query);
			rset = pstmt.executeQuery();

			memberList = new ArrayList<>();
			while(rset.next()) {
				MemberDTO member = new MemberDTO();
				member.setId(rset.getString("USER_ID"));
				member.setPwd(rset.getString("USER_PWD"));
				member.setEmail(rset.getString("USER_EMAIL"));
				member.setName(rset.getString("USER_NAME"));
				member.setSaveDate(rset.getString("SAVE_DATE"));
				member.setLife(rset.getInt("LIFE"));

				memberList.add(member);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}

		return memberList;
	}

	public List<String> selectId(Connection con) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		List<String> idList = null;

		String query = prop.getProperty("selectUser");

		MemberDTO member = new MemberDTO();

		try {
			pstmt = con.prepareStatement(query);
			rset = pstmt.executeQuery();
			
			idList = new ArrayList<>();

			while(rset.next()) {

				member.setId(rset.getString("USER_ID"));
				member.setPwd(rset.getString("USER_PWD"));
				member.setEmail(rset.getString("USER_EMAIL"));
				member.setName(rset.getString("USER_NAME"));
				member.setSaveDate(rset.getString("SAVE_DATE"));
				member.setLife(rset.getInt("LIFE"));

				idList.add(member.getId());
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}

		return idList;
	}

	public List<String> selectPwd(Connection con) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		List<String> pwdList = null;

		String query = prop.getProperty("selectUser");

		MemberDTO member = new MemberDTO();

		try {
			pstmt = con.prepareStatement(query);
			rset = pstmt.executeQuery();

			while(rset.next()) {

				member.setId(rset.getString("USER_ID"));
				member.setPwd(rset.getString("USER_PWD"));
				member.setEmail(rset.getString("USER_EMAIL"));
				member.setName(rset.getString("USER_NAME"));
				member.setSaveDate(rset.getString("SAVE_DATE"));
				member.setLife(rset.getInt("LIFE"));

				pwdList.add(member.getPwd());
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}

		return pwdList;
	}


	public String findUserId(Connection con, String name, String email) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;

		String findId = null;

		String query = prop.getProperty("findUserId");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, name);
			pstmt.setString(2, email);

			rset = pstmt.executeQuery();

			if(rset.next()) {
				findId = rset.getString("USER_ID");
			}


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return findId;

	}

	public String findUserPwd(Connection con, String name, String id, String email) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;

		String findPwd = null;

		String query = prop.getProperty("findUserId");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, name);
			pstmt.setString(2, id);
			pstmt.setString(3, email);

			rset = pstmt.executeQuery();

			if(rset.next()) {
				findPwd = rset.getString("USER_PWD");
			}


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return findPwd;
	}
	public boolean userWithdrawal(Connection con, String id, String pwd) {
		
		PreparedStatement pstmt = null;
		
		String query = prop.getProperty("userWithdrawal");
		
		int result = 0;
		
		boolean withdrawalResult = false;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, id);
			pstmt.setString(2, pwd);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(con);
		}
		
		if(result > 0) {
			withdrawalResult = true;
			return withdrawalResult;
		} else {
			return withdrawalResult;
		}
	}




>>>>>>> src/com/rodong/jdtc/member/model/dao/MemberDAO.java
}
