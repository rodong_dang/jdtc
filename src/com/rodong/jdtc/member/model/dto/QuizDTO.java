package com.rodong.jdtc.member.model.dto;

/* Quiz에 대한 DTO */
public class QuizDTO {

	private int quizNo;
	private String quizContent;
	private int QuizAnswerNo;
	private String quizAnswer;
	private String quizType;
	private int quizDifficultyCode;
	private String quizDifficultyName;
	private int selectByQuizNo;
	private int content_No;
	private String content_View;

	public QuizDTO() {
	}

	public QuizDTO(int quizNo, String quizContent, int quizAnswerNo, String quizAnswer, String quizType,
			int quizDifficultyCode, String quizDifficultyName, int selectByQuizNo, int content_No,
			String content_View) {
		super();
		this.quizNo = quizNo;
		this.quizContent = quizContent;
		QuizAnswerNo = quizAnswerNo;
		this.quizAnswer = quizAnswer;
		this.quizType = quizType;
		this.quizDifficultyCode = quizDifficultyCode;
		this.quizDifficultyName = quizDifficultyName;
		this.selectByQuizNo = selectByQuizNo;
		this.content_No = content_No;
		this.content_View = content_View;
	}

	public int getQuizNo() {
		return quizNo;
	}

	public void setQuizNo(int quizNo) {
		this.quizNo = quizNo;
	}

	public String getQuizContent() {
		return quizContent;
	}

	public void setQuizContent(String quizContent) {
		this.quizContent = quizContent;
	}

	public int getQuizAnswerNo() {
		return QuizAnswerNo;
	}

	public void setQuizAnswerNo(int quizAnswerNo) {
		QuizAnswerNo = quizAnswerNo;
	}

	public String getQuizAnswer() {
		return quizAnswer;
	}

	public void setQuizAnswer(String quizAnswer) {
		this.quizAnswer = quizAnswer;
	}

	public String getQuizType() {
		return quizType;
	}

	public void setQuizType(String quizType) {
		this.quizType = quizType;
	}

	public int getQuizDifficultyCode() {
		return quizDifficultyCode;
	}

	public void setQuizDifficultyCode(int quizDifficultyCode) {
		this.quizDifficultyCode = quizDifficultyCode;
	}

	public String getQuizDifficultyName() {
		return quizDifficultyName;
	}

	public void setQuizDifficultyName(String quizDifficultyName) {
		this.quizDifficultyName = quizDifficultyName;
	}

	public int getSelectByQuizNo() {
		return selectByQuizNo;
	}

	public void setSelectByQuizNo(int selectByQuizNo) {
		this.selectByQuizNo = selectByQuizNo;
	}

	public int getContent_No() {
		return content_No;
	}

	public void setContent_No(int content_No) {
		this.content_No = content_No;
	}

	public String getContent_View() {
		return content_View;
	}

	public void setContent_View(String content_View) {
		this.content_View = content_View;
	}

	@Override
	public String toString() {
		return "QuizDTO [quizNo=" + quizNo + ", quizContent=" + quizContent + ", QuizAnswerNo=" + QuizAnswerNo
				+ ", quizAnswer=" + quizAnswer + ", quizType=" + quizType + ", quizDifficultyCode=" + quizDifficultyCode
				+ ", quizDifficultyName=" + quizDifficultyName + ", selectByQuizNo=" + selectByQuizNo + ", content_No="
				+ content_No + ", content_View=" + content_View + "]";
	}
	
	
	
}