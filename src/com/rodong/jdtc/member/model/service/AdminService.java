package com.rodong.jdtc.member.model.service;

import static com.rodong.common.JDBCTemplate.getConnection;

import java.sql.Connection;
import java.util.List;

import com.rodong.jdtc.member.model.dao.MemberDAO;
import com.rodong.jdtc.member.model.dto.MemberDTO;

public class AdminService {

	private MemberDAO memberDAO;

	public List<MemberDTO> selectAllMember() {
		List<MemberDTO> selectAllMemberList = null;
		
		Connection con = getConnection();
		
		memberDAO = new MemberDAO();
		selectAllMemberList = memberDAO.selectAllMember(con);
		
		return selectAllMemberList;
	}	
	
}
