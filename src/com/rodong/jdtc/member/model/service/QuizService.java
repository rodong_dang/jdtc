package com.rodong.jdtc.member.model.service;

import static com.rodong.common.JDBCTemplate.close;
import static com.rodong.common.JDBCTemplate.commit;
import static com.rodong.common.JDBCTemplate.getConnection;
import static com.rodong.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.List;

import com.rodong.jdtc.member.model.dao.QuizDAO;
import com.rodong.jdtc.member.model.dto.QuizDTO;

/* Quiz에 대한 Service */
public class QuizService {


private final QuizDAO quizDAO;
	
	public QuizService() {
		this.quizDAO = new QuizDAO();
	}

	/* 모든퀴즈 조회 */
	public List<QuizDTO> selectAllQuiz() {

		Connection con = getConnection();

		List<QuizDTO> quizList = quizDAO.selectAllQuiz(con);

		close(con);

		return quizList;
	}

	/* 퀴즈 생성 */
	public int insertQuiz(QuizDTO quiz){
		Connection con = getConnection();
		int result = 0;
		result = quizDAO.insertQuiz(con, quiz);
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		return result;
		
		
		
	}

	/* 퀴즈넘버로 퀴즈내용 조회 */
	public QuizDTO selectByQuizNo(int quizNo) {
		Connection con = getConnection();
		QuizDTO quiz = quizDAO.selectByQuizNo(con, quizNo);
		
		close(con);
		
		return quiz;
	}
	
	/* 난이도명으로 퀴즈넘버 조회*/
	public List<QuizDTO> selectQuizNoByDifficultName(String diffString) {
		Connection con = getConnection();
		
		List<QuizDTO> quizList = quizDAO.selectQuizNoByDifficultName(con, diffString);
		
		close(con);
				
		return quizList;
	}

	/* 퀴즈넘버로 정답조회*/
	public List<QuizDTO> selectAnswerContentByQuizNo(int quizNo) {
		Connection con = getConnection();
		
		List<QuizDTO> quizList = quizDAO.selectAnswerContentByQuizNo(con, quizNo);
		
		close(con);
				
		return quizList;
	}

	/* 퀴즈번호로 퀴즈정답 조회*/
	public QuizDTO selectCompareAnswer(int quizNo) {
		Connection con = getConnection();
		QuizDTO quiz = quizDAO.selectCompareAnswer(con, quizNo);
		
		close(con);
		
		return quiz;
	}

}
