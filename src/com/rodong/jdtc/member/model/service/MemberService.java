package com.rodong.jdtc.member.model.service;

import java.sql.Connection;
import java.util.List;

import static com.rodong.common.JDBCTemplate.getConnection;
import static com.rodong.common.JDBCTemplate.close;
import static com.rodong.common.JDBCTemplate.commit;
import static com.rodong.common.JDBCTemplate.rollback;


import com.rodong.jdtc.member.model.dao.MemberDAO;
import com.rodong.jdtc.member.model.dto.MemberDTO;

/* Member(Account)에 대한 Service */
public class MemberService {

	/* MemberDAO 초기 생성자 생성, private 변수로 선언하여 직접 접근 불가 */
	private MemberDAO memberDAO;

	public MemberService() {
		this.memberDAO = new MemberDAO();
	}
	
	/* 회원가입 시 필요 메소드 */
	public int signUpUser(MemberDTO member) {
		
		Connection con = getConnection();
		
		int result = 0;
		
		int signUpResult = memberDAO.SignUpUser(con, member);

		
		if(signUpResult > 0) {
			result = 1;
			commit(con);
		} else {
			rollback(con);
		}

		close(con);
		
		return signUpResult;
	}

	
	public String findUserId(String name, String email) {
		
		Connection con = getConnection();
		
		String findId = memberDAO.findUserId(con, name, email);
		
		
		return findId;
	}
	
	public String findUserPwd(String name, String id, String email) {
		
		Connection con = getConnection();
		
		String findPwd = memberDAO.findUserPwd(con, name, id, email);
		
		return findPwd;
	}

	/* 로그인 시 아이디 조회용 메소드 */
	public List<String> selectId() {

		Connection con = getConnection();

		List<String> idList = memberDAO.selectId(con);

		close(con);

		return idList;
	}

	/* 로그인 시 비밀번호 조회용 메소드 */
	public List<String> selectPwd() {

		Connection con = getConnection();

		List<String> pwdList = memberDAO.selectPwd(con);

		close(con);

		return pwdList;
	}

	public boolean userWithdrawal(String id, String pwd) {

		Connection con = getConnection();
		
		boolean result = memberDAO.userWithdrawal(con, id, pwd);
		
		return result;
	}
}









