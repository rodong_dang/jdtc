package com.rodong.jdtc.member.view.implement;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class PanelSwitch {
	
	public static void changePanel(JFrame mf, JPanel oldPanel, JPanel newPanel) {
		mf.remove(oldPanel);
		mf.add(newPanel);
		mf.repaint();
		mf.revalidate();
	}
}
