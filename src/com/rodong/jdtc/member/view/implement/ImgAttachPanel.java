package com.rodong.jdtc.member.view.implement;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ImgAttachPanel extends JPanel{
   private JFrame mf;
   private JPanel panel;

   public ImgAttachPanel(JFrame mf, String icon) {

      this.mf = mf;
      panel = this;
      this.setBounds(-10, -20, 1400, 800);

      JLabel label = new JLabel(new ImageIcon(new ImageIcon(icon).getImage().getScaledInstance(1385, 760, 0)));
      label.setBounds(-10, -20, 1400, 800);

      this.add(label);

      mf.add(this);
   } 
}

