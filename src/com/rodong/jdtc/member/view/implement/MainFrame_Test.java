package com.rodong.jdtc.member.view.implement;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import com.rodong.jdtc.member.view.menu.AdminMenu;

public class MainFrame_Test extends JFrame{

	public MainFrame_Test(){
		this.setBounds(0, 0, 1400, 800);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		//this.setLayout(null);

		this.add(new AdminMenu(this));

		try {
			this.setIconImage(ImageIO.read(new File("Img/icon/icon.PNG"))); // 왼쪽이미지로고 사진바꾸는거ㅇ
		} catch (IOException e) {
			e.printStackTrace();

		}
		//this.add(new GraphicTest(this));

		this.setTitle("마왕 김진호를 JAVA");
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
