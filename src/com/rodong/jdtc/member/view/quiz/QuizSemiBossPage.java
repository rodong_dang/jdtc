package com.rodong.jdtc.member.view.quiz;

import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collections;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.rodong.jdtc.member.controller.QuizController;
import com.rodong.jdtc.member.view.implement.PanelSwitch;
import com.rodong.jdtc.member.view.sublist.SelectLevelPage;

/**
* <pre>
* Class : Quiz
* Comment : 중간보스들을 만났을 때의 화면 구현 메소드들을 모아놓은 클래
* History
* 2021/12/14 이태준 처음 작성
* 2021/12/19 이태준 대전 시작 창, 대사 창, 퀴즈 창, 승리 시 대사 화면, 패배 시 대사 화면 구현 
* 2021/12/20 이태준 마무리
* </pre>
* @version 1.5.0
* @author 이태준
*/
public class QuizSemiBossPage extends JPanel{
	
	/** '마왕 김진호를 JAVA'라는 이름의 프레임 생성 */
	JFrame jf = new JFrame("마왕 김진호를 JAVA");
	/** 일반 몬스터 화면 정보가 담긴 클래스의 인스턴스 생성 */
	QuizNormalPage qnp = new QuizNormalPage();
	/** 마왕 화면 정보가 담긴 클래스의 인스턴스 생성 */
	QuizKingPage qbp = new QuizKingPage();
	/** QuizController 클래스의 인스턴스 생성*/
	QuizController qc = new QuizController();
	/** 단계 확인 변수 */
	private int floorCount;
	/** 퀴즈 난이도를 나타내는 변수 */
	private String difficultyName;
	
	/**
	 * 단계에 대한 정보를 전달받아 그에 맞는 난이도 문제를 조회하는 메소드
	 * @param floorCount 현재의 단계를 알려주는 변수
	 */
	public void floorDifficultAppend(int floorCount) {
		if (floorCount <= 10) {
			difficultyName = "하";
			Collections.shuffle(qc.selectByDifficultName(difficultyName));

		} else if (floorCount > 10 && floorCount <= 20) {
			difficultyName = "중";
			Collections.shuffle(qc.selectByDifficultName(difficultyName));

		} else if (floorCount > 20 && floorCount <= 29) {
			difficultyName = "상";
			Collections.shuffle(qc.selectByDifficultName(difficultyName));
		}
	}
	
	/** 첫 번째 중간보스를 만났을 때 대결 창을 보여주는 메소드 */
	public void semiBossVsPanel1() {
		this.floorCount += 5;
		
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/semiBoss/MB1-vs.png").getImage().getScaledInstance(1385, 760, 0);
		
		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);
		jf.add(panel);

		/** panel 클릭 시 발생하는 이벤트 */
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				/** 프레임에서 사용한 panel 삭제 */
				jf.remove(panel);
				
				semiBossDialogue1_1();
			}
		});

		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	/** 두 번째 중간보스를 만났을 때 대결 창을 보여주는 메소드 */
	public void semiBossVsPanel2() {
		this.floorCount += 5;
		
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/semiBoss/MB2-vs.png").getImage().getScaledInstance(1385, 760, 0);
		
		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);
		jf.add(panel);

		/** panel 클릭 시 발생하는 이벤트 */
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				/** 프레임에서 사용한 panel 삭제 */
				jf.remove(panel);
				
				semiBossDialogue2_1();
			}
		});

		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	/** 세 번째 중간보스를 만났을 때 대결 창을 보여주는 메소드 */
	public void semiBossVsPanel3() {
		this.floorCount += 5;
		
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/semiBoss/MB3-vs.png").getImage().getScaledInstance(1385, 760, 0);
		
		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);
		jf.add(panel);

		/** panel 클릭 시 발생하는 이벤트 */
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				/** 프레임에서 사용한 panel 삭제 */
				jf.remove(panel);
				
				semiBossDialogue3_1();
			}
		});

		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	/** 네 번째 중간보스를 만났을 때 대결 창을 보여주는 메소드 */
	public void semiBossVsPanel4() {
		this.floorCount += 5;
		
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/semiBoss/MB4-vs.png").getImage().getScaledInstance(1385, 760, 0);
		
		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);
		jf.add(panel);

		/** panel 클릭 시 발생하는 이벤트 */
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				/** 프레임에서 사용한 panel 삭제 */
				jf.remove(panel);
				
				semiBossDialogue4_1();
			}
		});

		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	/** 다섯 번째 중간보스를 만났을 때 대결 창을 보여주는 메소드 */
	public void semiBossVsPanel5() {
		this.floorCount += 5;
		
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/semiBoss/MB5-vs.png").getImage().getScaledInstance(1385, 760, 0);
		
		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);
		jf.add(panel);

		/** panel 클릭 시 발생하는 이벤트 */
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				/** 프레임에서 사용한 panel 삭제 */
				jf.remove(panel);
				
				semiBossDialogue5_1();
			}
		});

		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	/** 퀴즈 풀기 전 첫 중간보스의 대사 */
	public void semiBossDialogue1_1() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB1-Dialogue1.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossDialogue1_2();
			}
		});
	}

	public void semiBossDialogue1_2() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB1-Dialogue2.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossDialogue1_3();
			}
		});
	}

	public void semiBossDialogue1_3() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB1-Dialogue3.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				semiBossQuiz1();
			}
		});
	}
	
	/** 퀴즈 풀기 전 두 번째 중간보스 대사 */
	public void semiBossDialogue2_1() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB2-Dialogue1.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossQuiz2();
			}
		});
	}
	
	/** 퀴즈 풀기 전 세 번째 중간보스 대사 */
	public void semiBossDialogue3_1() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB3-Dialogue1.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossDialogue3_2();
			}
		});
	}
	
	public void semiBossDialogue3_2() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB3-Dialogue2.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossQuiz3();
			}
		});
	}
	
	/** 퀴즈 풀기 전 네 번째 중간보스 대사 */
	public void semiBossDialogue4_1() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB4-Dialogue1.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossDialogue4_2();
			}
		});
	}
	
	public void semiBossDialogue4_2() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB4-Dialogue2.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossQuiz4();
			}
		});
	}
	
	/** 퀴즈 풀기 전 다섯 번째 중간보스 대사 */
	public void semiBossDialogue5_1() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB5-Dialogue1.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossDialogue5_2();
			}
		});
	}
	
	public void semiBossDialogue5_2() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB5-Dialogue2.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossDialogue5_3();
			}
		});
	}
	
	public void semiBossDialogue5_3() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB5-Dialogue3.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossQuiz5();
			}
		});
	}
	
	/** 첫 번째 중간보스가 출제하는 퀴즈 */
	public void semiBossQuiz1() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB1-bg.png").getImage().getScaledInstance(1385, 760, 0);
		Image contentImg = new ImageIcon("img/SemiBoss/content.png").getImage().getScaledInstance(1000, 200, 0);
		Image enemyImg = new ImageIcon("img/SemiBoss/smallMB1.png").getImage().getScaledInstance(300, 300, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));
		JLabel content = new JLabel(new ImageIcon(contentImg));
		JLabel enemy = new JLabel(new ImageIcon(enemyImg));

		panel.setLayout(null);
		panel.add(enemy).setBounds(1150, 490, 230, 250);
		panel.add(content).setBounds(200, 500, 1000, 200);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				/** 이겼을 경우 */
//				semiBossWin1_1();
				
				/** 졌을 경우 */
				semiBossLose1_1();
			}
		});
	}
	
	/** 두 번째 중간보스가 출제하는 퀴즈 */
	public void semiBossQuiz2() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB2-bg.png").getImage().getScaledInstance(1385, 760, 0);
		Image contentImg = new ImageIcon("img/SemiBoss/content.png").getImage().getScaledInstance(1000, 200, 0);
		Image enemyImg = new ImageIcon("img/SemiBoss/smallMB2.png").getImage().getScaledInstance(300, 300, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));
		JLabel content = new JLabel(new ImageIcon(contentImg));
		JLabel enemy = new JLabel(new ImageIcon(enemyImg));

		panel.setLayout(null);
		panel.add(enemy).setBounds(1150, 490, 230, 250);
		panel.add(content).setBounds(200, 500, 1000, 200);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				/** 이겼을 경우 */
				semiBossWin2_1();
				
				/** 졌을 경우 */
//				semiBossLose2_1();
			}
		});
	}
	
	/** 세 번째 중간보스가 출제하는 퀴즈 */
	public void semiBossQuiz3() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB3-bg.png").getImage().getScaledInstance(1385, 760, 0);
		Image contentImg = new ImageIcon("img/SemiBoss/content.png").getImage().getScaledInstance(1000, 200, 0);
		Image enemyImg = new ImageIcon("img/SemiBoss/smallMB3.png").getImage().getScaledInstance(300, 300, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));
		JLabel content = new JLabel(new ImageIcon(contentImg));
		JLabel enemy = new JLabel(new ImageIcon(enemyImg));

		panel.setLayout(null);
		panel.add(enemy).setBounds(1150, 490, 230, 250);
		panel.add(content).setBounds(200, 500, 1000, 200);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				/** 이겼을 경우 */
				semiBossWin3_1();
				
				/** 졌을 경우 */
//				semiBossLose3_1();
			}
		});
	}
	
	/** 네 번째 중간보스가 출제하는 퀴즈 */
	public void semiBossQuiz4() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB4-bg.png").getImage().getScaledInstance(1385, 760, 0);
		Image contentImg = new ImageIcon("img/SemiBoss/content.png").getImage().getScaledInstance(1000, 200, 0);
		Image enemyImg = new ImageIcon("img/SemiBoss/smallMB4.png").getImage().getScaledInstance(300, 300, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));
		JLabel content = new JLabel(new ImageIcon(contentImg));
		JLabel enemy = new JLabel(new ImageIcon(enemyImg));

		panel.setLayout(null);
		panel.add(enemy).setBounds(1150, 490, 230, 250);
		panel.add(content).setBounds(200, 500, 1000, 200);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				/** 이겼을 경우 */
				semiBossWin4_1();
				
				/** 졌을 경우 */
//				semiBossLose4_1();
			}
		});
	}
	
	/** 다섯 번째 중간보스가 출제하는 퀴즈 */
	public void semiBossQuiz5() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB5-bg.png").getImage().getScaledInstance(1385, 760, 0);
		Image contentImg = new ImageIcon("img/SemiBoss/content.png").getImage().getScaledInstance(1000, 200, 0);
		Image enemyImg = new ImageIcon("img/SemiBoss/smallMB5.png").getImage().getScaledInstance(300, 300, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));
		JLabel content = new JLabel(new ImageIcon(contentImg));
		JLabel enemy = new JLabel(new ImageIcon(enemyImg));

		panel.setLayout(null);
		panel.add(enemy).setBounds(1150, 490, 230, 250);
		panel.add(content).setBounds(200, 500, 1000, 200);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				/** 이겼을 경우 */
				semiBossWin5_1();
				
				/** 졌을 경우 */
//				semiBossLose5_1();
			}
		});
	}
	
	/** 첫 번째 중간보스와의 전투 승리 시 중간보스의 대사 및 화면 */
	public void semiBossWin1_1() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB1-Win1.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossWin1_2();
			}
		});
	}
	
	public void semiBossWin1_2() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB1-Win2.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossWin1_3();
			}
		});
	}
	
	public void semiBossWin1_3() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB1-Win3.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossWin1_4();
			}
		});
	}
	
	public void semiBossWin1_4() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB1-Win4.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));
		JButton stepButton = new JButton("단계선택 이동");
		JButton nextButton = new JButton("다음단계 이동");

		panel.setLayout(null);
		panel.add(stepButton).setBounds(100, 650, 200, 50);
		panel.add(nextButton).setBounds(1100, 650, 200, 50);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		stepButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				/**
				 * 단계 선택창으로 이동 : 화면전환
				 * @param 사용하고 있는 프레임
				 * @param 바꿔주려고 하는 기존 panel
				 * @param 새로운 panel
				 */
//				PanelSwitch.changePanel(jf, panel, new SelectLevelPage(jf));
			}
		});

		nextButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				/** 다음 단계로 이동 : 화면전환 */
				qnp.round2VsPanel();
			}
		});
	}
	
	/** 두 번째 중간보스와의 전투 승리 시 중간보스의 대사 및 화면 */
	public void semiBossWin2_1() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB2-Win1.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossWin2_2();
			}
		});
	}
	
	public void semiBossWin2_2() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB2-Win2.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossWin2_3();
			}
		});
	}
	
	public void semiBossWin2_3() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB2-Win3.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossWin2_4();
			}
		});
	}
	
	public void semiBossWin2_4() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB2-Win4.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));
		JButton stepButton = new JButton("단계선택 이동");
		JButton nextButton = new JButton("다음단계 이동");

		panel.setLayout(null);
		panel.add(stepButton).setBounds(100, 650, 200, 50);
		panel.add(nextButton).setBounds(1100, 650, 200, 50);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		stepButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				/** 단계 선택창으로 이동 : 화면전환 */
//				PanelSwitch.changePanel(jf, panel, new SelectLevelPage(jf));
			}
		});

		nextButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				/** 다음 단계로 이동 : 화면전환 */
				qnp.round3VsPanel();
			}
		});
	}
	
	/** 세 번째 중간보스와의 전투 승리 시 중간보스의 대사 및 화면 */
	public void semiBossWin3_1() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB3-Win1.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossWin3_2();
			}
		});
	}
	
	public void semiBossWin3_2() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB3-Win2.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossWin3_3();
			}
		});
	}
	
	public void semiBossWin3_3() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB3-Win3.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossWin3_4();
			}
		});
	}
	
	public void semiBossWin3_4() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB3-Win4.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));
		JButton stepButton = new JButton("단계선택 이동");
		JButton nextButton = new JButton("다음단계 이동");

		panel.setLayout(null);
		panel.add(stepButton).setBounds(100, 650, 200, 50);
		panel.add(nextButton).setBounds(1100, 650, 200, 50);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		stepButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				/** 단계 선택창으로 이동 : 화면전환 */
//				PanelSwitch.changePanel(jf, panel, new SelectLevelPage(jf));
			}
		});

		nextButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				/** 다음 단계로 이동 : 화면전환 */
				qnp.round4VsPanel();
			}
		});
	}
	
	/** 네 번째 중간보스와의 전투 승리 시 중간보스의 대사 및 화면 */
	public void semiBossWin4_1() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB4-Win1.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossWin4_2();
			}
		});
	}
	
	public void semiBossWin4_2() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB4-Win2.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossWin4_3();
			}
		});
	}
	
	public void semiBossWin4_3() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB4-Win3.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossWin4_4();
			}
		});
	}
	
	public void semiBossWin4_4() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB4-Win4.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));
		JButton stepButton = new JButton("단계선택 이동");
		JButton nextButton = new JButton("다음단계 이동");

		panel.setLayout(null);
		panel.add(stepButton).setBounds(100, 650, 200, 50);
		panel.add(nextButton).setBounds(1100, 650, 200, 50);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		stepButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				/** 단계 선택창으로 이동 : 화면전환 */
//				PanelSwitch.changePanel(jf, panel, new SelectLevelPage(jf));
			}
		});

		nextButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				/** 다음 단계로 이동 : 화면전환 */
				qnp.round5VsPanel();
			}
		});
	}
	
	/** 다섯 번째 중간보스와의 전투 승리 시 중간보스의 대사 및 화면 */
	public void semiBossWin5_1() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB5-Win1.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossWin5_2();
			}
		});
	}
	
	public void semiBossWin5_2() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB5-Win2.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossWin5_3();
			}
		});
	}
	
	public void semiBossWin5_3() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB5-Win3.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossWin5_4();
			}
		});
	}
	
	public void semiBossWin5_4() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB5-Win4.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));
		JButton stepButton = new JButton("단계선택 이동");
		JButton nextButton = new JButton("다음단계 이동");

		panel.setLayout(null);
		panel.add(stepButton).setBounds(100, 650, 200, 50);
		panel.add(nextButton).setBounds(1100, 650, 200, 50);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		stepButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				/** 단계 선택창으로 이동 : 화면전환 */
//				PanelSwitch.changePanel(jf, panel, new SelectLevelPage(jf));
			}
		});

		nextButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				/** 다음 단계로 이동 : 화면전환 */
				qnp.round6VsPanel();
			}
		});
	}
	
	/** 첫 번째 중간보스와의 전투 패배 시 중간보스의 대사 및 화면 */
	public void semiBossLose1_1() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB1-Lose1.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));
		JButton stepButton = new JButton("단계 선택 이동");

		panel.setLayout(null);
		panel.add(stepButton).setBounds(100, 650, 200, 50);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		stepButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				/** 단계 선택창으로 이동 : 화면전환 */
//				PanelSwitch.changePanel(jf, panel, new SelectLevelPage(jf));
			}
		});
	}
	
	/** 두 번째 중간보스와의 전투 패배 시 중간보스의 대사 및 화면 */
	public void semiBossLose2_1() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB2-Lose1.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));
		JButton stepButton = new JButton("단계 선택 이동");

		panel.setLayout(null);
		panel.add(stepButton).setBounds(100, 650, 200, 50);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		stepButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				/** 단계 선택창으로 이동 : 화면전환 */
//				PanelSwitch.changePanel(jf, panel, new SelectLevelPage(jf));
			}
		});
	}
	
	/** 세 번째 중간보스와의 전투 패배 시 중간보스의 대사 및 화면 */
	public void semiBossLose3_1() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB3-Lose1.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));
		JButton stepButton = new JButton("단계 선택 이동");

		panel.setLayout(null);
		panel.add(stepButton).setBounds(100, 650, 200, 50);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		stepButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				/** 단계 선택창으로 이동 : 화면전환 */
//				PanelSwitch.changePanel(jf, panel, new SelectLevelPage(jf));
			}
		});
	}
	
	/** 네 번째 중간보스와의 전투 패배 시 중간보스의 대사 및 화면 */
	public void semiBossLose4_1() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB4-Lose1.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();

		panel.setLayout(null);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossLose4_2();
			}
		});
	}
	
	public void semiBossLose4_2() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB4-Lose2.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));
		JButton stepButton = new JButton("단계 선택 이동");

		panel.setLayout(null);
		panel.add(stepButton).setBounds(100, 650, 200, 50);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		stepButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				/** 단계 선택창으로 이동 : 화면전환 */
//				PanelSwitch.changePanel(jf, panel, new SelectLevelPage(jf));
			}
		});
	}
	
	/** 다섯 번째 중간보스와의 전투 패배 시 중간보스의 대사 및 화면 */
	public void semiBossLose5_1() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB5-Lose1.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();

		panel.setLayout(null);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				
				semiBossLose5_2();
			}
		});
	}
	
	public void semiBossLose5_2() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/SemiBoss/MB5-Lose2.png").getImage().getScaledInstance(1385, 760, 0);

		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));
		JButton stepButton = new JButton("단계 선택 이동");

		panel.setLayout(null);
		panel.add(stepButton).setBounds(100, 650, 200, 50);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		stepButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				/** 단계 선택창으로 이동 : 화면전환 */
//				PanelSwitch.changePanel(jf, panel, new SelectLevelPage(jf));
			}
		});
	}
	
}