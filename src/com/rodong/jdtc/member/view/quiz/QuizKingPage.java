package com.rodong.jdtc.member.view.quiz;

import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class QuizKingPage  {
	
	JFrame jf = new JFrame("마왕 김진호를 JAVA");
	int result = 1;
	
	public QuizKingPage() {
		
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/KingJH/kingjh1vs.png").getImage().getScaledInstance(1385, 760, 0);
		
		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));

		panel.setLayout(null);

		panel.add(bg).setBounds(-10, -20, 1400, 800);
		jf.add(panel);

		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				MtPanel();
			}
		});

		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}		
	
	public void MtPanel() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/KingJH/kingjh2mt.png").getImage().getScaledInstance(1385, 760, 0);
		
		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));


		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				QzPanel();
			}
		});
	}
	
	public void QzPanel() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/KingJH/kingjh3qz.png").getImage().getScaledInstance(1385, 760, 0);
		
		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));


		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				QztPanel();
			}
		});
	}
	
	public void QztPanel() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/KingJH/kingjh4qz.png").getImage().getScaledInstance(1385, 760, 0);
		
		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));


		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				LosePanel();
			}
		});
	}
	
	public void LosePanel() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/KingJH/kingjh5ls.png").getImage().getScaledInstance(1385, 760, 0);
		
		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));


		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				WinPanel();
			}
		});
	}
	
	public void WinPanel() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/KingJH/kingjh6win.png").getImage().getScaledInstance(1385, 760, 0);
		
		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));


		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				WintPanel();
			}
		});
	}
	
	public void WintPanel() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/KingJH/kingjh7win.png").getImage().getScaledInstance(1385, 760, 0);
		
		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));


		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				WinstPanel();
			}
		});
	}
	
	public void WinstPanel() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/KingJH/kingjh8win.png").getImage().getScaledInstance(1385, 760, 0);
		
		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));


		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				StPanel();
			}
		});
	}
	
	public void StPanel() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/KingJH/kingjh9st.png").getImage().getScaledInstance(1385, 760, 0);
		
		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));


		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				SSSPanel();
			}
		});
	}
	
	public void SSSPanel() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/KingJH/kingjh10ss.png").getImage().getScaledInstance(1385, 760, 0);
		
		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));


		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				SttPanel();
			}
		});
	}
	
	public void SttPanel() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/KingJH/kingjh10ss.png").getImage().getScaledInstance(1385, 760, 0);
		
		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));


		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
				VPanel();
			}
		});
	}
	
	public void VPanel() {
		jf.setSize(1400, 800);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);

		Image bgImg = new ImageIcon("img/KingJH/kingjh11v.png").getImage().getScaledInstance(1385, 760, 0);
		
		JPanel panel = new JPanel();
		JLabel bg = new JLabel(new ImageIcon(bgImg));


		panel.setLayout(null);
		panel.add(bg).setBounds(-10, -20, 1400, 800);

		jf.add(panel);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jf.remove(panel);
			}
		});
	}
}

	
