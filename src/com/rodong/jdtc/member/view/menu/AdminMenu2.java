package com.rodong.jdtc.member.view.menu;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.rodong.jdtc.member.view.implement.ImgAttachPanel;
import com.rodong.jdtc.member.view.implement.MainFrame_Test;

/* 관리자 모드 button 클릭 시 나오는 AdminMenu
 * 팝업으로 진입???? */
public class AdminMenu2 extends JPanel {

	
	private JFrame mf;
	private JPanel panel;
	private JPanel UserManage;
	//	private JPanel selectyById;
//	Member_List ml = new Member_List();

	//	private JPanel UserManage;

	public AdminMenu2(MainFrame_Test mf) {

		this.mf = mf;
		this.setLayout(null);
		ImgAttachPanel gp = null;

		panel = this;

		JLabel label = new JLabel(new ImageIcon(new ImageIcon("img/Menu/AdminBackground01.png").getImage().getScaledInstance(1385, 760, 0)));
		label.setBounds(-10, -20, 1400, 800); 

		/* 1. 회원 관리창 버튼 - 팝업 */
		JLabel button1 = new JLabel(new ImageIcon(new ImageIcon("img/button/AdminMenuButton.png").getImage().getScaledInstance(300, 100, 0)));
		button1.setBounds(500, 170, 300, 150);
		JLabel text1 = new JLabel("회원 관리");
		text1.setBounds(580, 195, 200, 100);
		text1.setFont(new Font("굴림체", Font.BOLD, 15));
		text1.setForeground(Color.white);
		text1.setFont(text1.getFont().deriveFont(30.0f));


		/* 2. 퀴즈 관리창 버튼 - 팝업 */
		JLabel button2 = new JLabel(new ImageIcon(new ImageIcon("img/button/AdminMenuButton.png").getImage().getScaledInstance(300, 100, 0)));
		button2.setBounds(500, 290, 300, 150);
		JLabel text2 = new JLabel("퀴즈 관리");
		text2.setBounds(580, 282, 200, 170);
		text2.setFont(new Font("굴림체", Font.BOLD, 15));
		text2.setForeground(Color.white);
		text2.setFont(text2.getFont().deriveFont(30.0f));

		/* 관리자 모드 종료 버튼 */
		JLabel button3 = new JLabel(new ImageIcon(new ImageIcon("img/button/AdminMenuButton.png").getImage().getScaledInstance(300, 100, 0)));
		button3.setBounds(500, 410, 300, 150);
		JLabel text3 = new JLabel("관리자모드 종료");
		text3.setBounds(570, 366, 200, 240);
		text3.setFont(new Font("굴림체", Font.BOLD, 15));
		text3.setForeground(Color.white);
		text3.setFont(text3.getFont().deriveFont(22.0f));

		JLabel button4 = new JLabel(new ImageIcon(new ImageIcon("img/button/AdminButton.gif").getImage().getScaledInstance(220, 80, 0)));
		button4.setBounds(22, 85, 400, 100);



		/* 1. 회원 관리창 버튼 입력시 나오는 화면*/
		button1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				/* 회원 관리 팝업창의 기본 프레임*/
				JFrame popUpFrame = new JFrame("회원 관리");										
				popUpFrame.setSize( 900, 600);
				popUpFrame.setLocationRelativeTo(null);
				
				/* 회원 관리 팝업창의 초기 버튼을 올릴 판넬*/
				JPanel UserManage = new JPanel();																
				UserManage.setLayout(null);
				UserManage.setSize(900, 600);
				
				/* 1-1. 회원 정보 조회 버튼 */
				JButton SelectUser = new JButton("회원 정보 조회");								
				SelectUser.setFont(SelectUser.getFont().deriveFont(15.0f));
				SelectUser.setLayout(null);
				SelectUser.setBounds(350, 50, 150, 40);

				/* 1-1. 회원 정보 조회 버튼 입력시 나오는 화면 */
				SelectUser.addMouseListener(new MouseAdapter() {
					@Override	
					public void mouseClicked(MouseEvent e) {
					}
				});




		button2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

			}
		});

		button3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

			}
		});




		this.add(label);
		this.add(button1);
		this.add(button2);
		this.add(button3);
		this.add(button4);

		this.setComponentZOrder(button1, 1);
		this.setComponentZOrder(text1, 0);
		this.setComponentZOrder(label, 2);
		this.setComponentZOrder(button2, 1);
		this.setComponentZOrder(text2, 0);
		this.setComponentZOrder(button3, 1);
		this.setComponentZOrder(text3, 0);
		this.setComponentZOrder(button4, 1);

	}
}


