package com.rodong.jdtc.member.view.menu;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.rodong.jdtc.member.view.implement.MainFrame_Test;
import com.rodong.jdtc.member.view.implement.PanelSwitch;
import com.rodong.jdtc.member.view.sublist.CharacterStatusPage;
import com.rodong.jdtc.member.view.sublist.StoryPage;

/* <게임설명, 캐릭터상태, 모험떠나기, 로그아웃> */

/* 게임설명 : 팝업
 * 캐릭터상태, 모험떠나기, 로그아웃, 게임설명 : 패널 전환    */

/* Main Menu (로그인 성공 시 출력되는 메인메뉴) */
public class MainMenu extends JPanel {

	private MainFrame_Test mf; // 공통 프레임을 불러오기 위한 전역변수 선언

	public MainMenu(MainFrame_Test mf) { 

		this.setSize(1400, 800);
		JPanel thisPanel = this; // 버튼 내에서 사용해주기 위해 현 패널을 this로 설정

		/* Image에 배경화면, 버튼(텍스트를 감싸고 있는)들의 이미지 삽입 */
		Image backgroundImage = new ImageIcon("img/Menu/MainPhoto1.PNG").getImage().getScaledInstance(1385, 760, 0);
		Image button1Image = new ImageIcon("img/button/RealButton.png").getImage().getScaledInstance(300, 100, 0);
		Image button2Image = new ImageIcon("img/button/RealButton.png").getImage().getScaledInstance(300, 100, 0);
		Image button3Image = new ImageIcon("img/button/RealButton.png").getImage().getScaledInstance(300, 100, 0);
		Image button4Image = new ImageIcon("img/button/RealButton.png").getImage().getScaledInstance(300, 100, 0);

		/* JLabel에 위에서 만든 Image들 각각 삽입 */
		JLabel bg = new JLabel(new ImageIcon(backgroundImage));
		JLabel button1 = new JLabel(new ImageIcon(button1Image));
		JLabel button2 = new JLabel(new ImageIcon(button2Image));
		JLabel button3 = new JLabel(new ImageIcon(button3Image));
		JLabel button4 = new JLabel(new ImageIcon(button4Image));

		/* 각 버튼내에 들어갈 text라벨을 생성 */
		JLabel text1 = new JLabel();
		text1.setForeground(Color.white);
		text1.setBounds(120, 145, 300, 100);
		text1.setFont(new Font("굴림체", Font.BOLD, 20));
		text1.setText("게임설명창");
		text1.setHorizontalAlignment(JLabel.CENTER); // 수평 가운데 정렬
		mf.getContentPane().add(text1); // 현 JPanel에 올려보았으나 이미지가 나오지 않아 프레임에 직접 올려줌

		JLabel text2 = new JLabel();
		text2.setForeground(Color.white);
		text2.setBounds(125, 295, 300, 100);
		text2.setFont(new Font("굴림체", Font.BOLD, 20));
		text2.setText("캐릭터상태창");
		text2.setHorizontalAlignment(JLabel.CENTER); // 수평 가운데 정렬
		mf.getContentPane().add(text2);

		JLabel text3 = new JLabel();
		text3.setForeground(Color.white);
		text3.setBounds(125, 445, 300, 100);
		text3.setFont(new Font("굴림체", Font.BOLD, 20));
		text3.setText("모험 떠나기");
		text3.setHorizontalAlignment(JLabel.CENTER); // 수평 가운데 정렬
		mf.getContentPane().add(text3);

		JLabel text4 = new JLabel();
		text4.setForeground(Color.white);
		text4.setBounds(1020, 645, 300, 100);
		text4.setFont(new Font("굴림체", Font.BOLD, 20));
		text4.setText("로그아웃");
		text4.setHorizontalAlignment(JLabel.CENTER); // 수평 가운데 정렬
		mf.getContentPane().add(text4);

		/* 각 버튼(이미지)들의 위치를 지정함과 동시에 현 JPanel에 add */
		this.add(button1).setBounds(100, 100, 350, 170);
		this.add(button2).setBounds(100, 250, 350, 170);
		this.add(button3).setBounds(100, 400, 350, 170);
		this.add(button4).setBounds(1000, 600, 350, 170);
		this.add(bg).setBounds(-10, -20, 1400, 800);

		this.setLayout(null);

		/* button(이미지)들을 선택했을 시 반응을 하도록 각각 액션리스너 설정 */
		button1.addMouseListener(new MouseAdapter() {

			/* 새로운 프레임을 생성하여 팝업창 구현 */
			@Override
			public void mouseClicked(MouseEvent e) {
				mf.setVisible(true);
				JFrame popUpFrame = new JFrame("게임설명창");
				popUpFrame.setBounds(520, 170, 425, 580);

				JPanel popUpPanel = new JPanel();
				popUpPanel.setBounds(480, 300, 415, 570);

				JLabel popUpLabel = new JLabel(new ImageIcon("img/popup/GameExplanationPopup.png"));
				popUpLabel.setBounds(480, 300, 415, 570);

				popUpPanel.add(popUpLabel);
				popUpFrame.add(popUpPanel);
				popUpFrame.setVisible(true);


			}
		});

		button2.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				mf.remove(text1);
				mf.remove(text2);
				mf.remove(text3);
				mf.remove(text4);
				mf.remove(button1);
				mf.remove(button2);
				mf.remove(button3);
				mf.remove(button4);
				PanelSwitch.changePanel(mf, thisPanel, new CharacterStatusPage(mf));

			}
		});

		button3.addMouseListener(new MouseAdapter() {  

			@Override
			public void mouseClicked(MouseEvent e) {
				mf.remove(text1);
				mf.remove(text2);
				mf.remove(text3);
				mf.remove(text4);
				mf.remove(button1);
				mf.remove(button2);
				mf.remove(button3);
				mf.remove(button4);
				PanelSwitch.changePanel(mf, thisPanel, new StoryPage(mf));

			}
		});

		button4.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				mf.remove(text1);
				mf.remove(text2);
				mf.remove(text3);
				mf.remove(text4);
				mf.remove(button1);
				mf.remove(button2);
				mf.remove(button3);
				mf.remove(button4);
			   PanelSwitch.changePanel(mf, thisPanel, new LoginMenu(mf));

			}
		});

		/* 현재 JPanel을 메인프레임에 올려주기 */
		mf.add(this);
	}
}
