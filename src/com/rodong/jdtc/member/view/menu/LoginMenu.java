package com.rodong.jdtc.member.view.menu;

import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.rodong.jdtc.member.controller.MemberController;
import com.rodong.jdtc.member.view.implement.MainFrame_Test;
import com.rodong.jdtc.member.view.implement.PanelSwitch;
import com.rodong.jdtc.member.view.sublist.StoryPage;

public class LoginMenu extends JPanel {
	
	private final MemberController mc;
	
	public LoginMenu(MainFrame_Test mf) {
		
		this.mc = new MemberController();
		
		/* 로그인페이지의 배경화면 구현 실패 */
//		Image loginBgImage = new ImageIcon(
//				"img/Menu/defaultBackground.png").getImage().getScaledInstance(1385, 760, 0);;
//		JLabel bgImageLabel = new JLabel();
		JPanel loginMenu = new JPanel();
		
		loginMenu.setSize(1400, 800);
		loginMenu.setBorder(null);
		loginMenu.setLayout(null);

		mf.getContentPane().add(loginMenu);
		
		/* 계정 찾기 버튼 */
		JButton findAccountBtn = new JButton(new ImageIcon("img/button/defaultButton.png"));
		JLabel findText = new JLabel("계정찾기");
		findText.setForeground(Color.WHITE);
		findText.setFont(new Font("돋움", Font.BOLD, 24));
		findAccountBtn.setBounds(1100, 39, 138, 76);
		findAccountBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Dialog loginDialog = new Dialog(mf, "계정 찾기");
				loginDialog.setVisible(true);
				
				JButton backBtn = new JButton("뒤로가기");
				JTextField idField = new JTextField();
				backBtn.setSize(200, 100);
				
				loginDialog.add(idField);
				loginDialog.add(backBtn);

				
				backBtn.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
					}
				});
			}
		});
		loginMenu.add(findAccountBtn);
		findAccountBtn.add(findText);
		
		/* 회원가입 버튼 */
		JButton signUpMemberBtn = new JButton(new ImageIcon("img/button/defaultButton.png"));
		JLabel signUpText = new JLabel("회원가입");
		signUpText.setForeground(Color.WHITE);
		signUpText.setFont(new Font("돋움", Font.BOLD, 24));
		signUpMemberBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String id = JOptionPane.showInputDialog("아이디 : ");
				String name = JOptionPane.showInputDialog("이름 : ");
				String pwd = JOptionPane.showInputDialog("비밀번호 : ");
				String pwd_again = JOptionPane.showInputDialog("비밀번호 확인 : ");
			}
		});
		signUpMemberBtn.setBounds(1100, 211, 138, 76);

		loginMenu.add(signUpMemberBtn);
		signUpMemberBtn.add(signUpText);

		/* 회원탈퇴 버튼 */		
		JButton withDrawalAccount = new JButton(new ImageIcon("img/button/defaultButton.png"));
		JLabel withDrawalText = new JLabel("회원탈퇴");
		withDrawalText.setForeground(Color.WHITE);
		withDrawalText.setFont(new Font("돋움", Font.BOLD, 24));
		withDrawalAccount.setBounds(1100, 395, 138, 76);
		withDrawalAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
		loginMenu.add(withDrawalAccount);
		withDrawalAccount.add(withDrawalText);

		/* 관리자모드 버튼 */		
		JButton adminMenuBtn = new JButton(new ImageIcon("img/button/defaultButton.png"));
		JLabel adminText = new JLabel("관리자모드");
		adminText.setForeground(Color.WHITE);
		adminText.setFont(new Font("돋움", Font.BOLD, 18));
		adminMenuBtn.setBounds(1100, 567, 138, 76);
		adminMenuBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PanelSwitch.changePanel(mf, loginMenu, new AdminMenu(mf));
			}
		});
		loginMenu.add(adminMenuBtn);
		adminMenuBtn.add(adminText);

		/* 게임종료 버튼 */		
		JButton gameQuitBtn = new JButton(new ImageIcon("img/button/defaultButton.png"));
		JLabel quitText = new JLabel("게임종료");
		quitText.setForeground(Color.WHITE);
		quitText.setFont(new Font("돋움", Font.BOLD, 24));
		gameQuitBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "게임을 종료합니다.");
				System.exit(0);
			}
		});
		gameQuitBtn.setBounds(100, 597, 138, 76);
		loginMenu.add(gameQuitBtn);
		gameQuitBtn.add(quitText);

		/* 로그인 패널 */
		JPanel loginPanel;
		ImageIcon loginPanelImage = new ImageIcon("img/Menu/loginPanelBackground.png");
		loginPanel = new JPanel(){
			public void paintComponent(Graphics g) {
				Dimension d = getSize();
				g.drawImage(loginPanelImage.getImage(), 0, 0, d.width, d.height, null);
				setOpaque(false);
				super.paintComponent(g);
			}
		};;
		
		loginPanel.setBounds(320, 45, 700, 570);
		loginMenu.add(loginPanel);
		loginPanel.setLayout(null);
		
		/* 로그인 패널 관련 JLabel과 TextField들 */
		JLabel lblNewLabel = new JLabel("마왕 김진호를 JAVA");
		lblNewLabel.setBounds(173, 107, 500, 50);
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("나눔고딕 ExtraBold", Font.BOLD, 36));
		loginPanel.add(lblNewLabel);

		JLabel textLabelId = new JLabel("아이디");
		textLabelId.setForeground(Color.WHITE);
		textLabelId.setFont(new Font("나눔고딕", Font.BOLD, 24));
		textLabelId.setBounds(126, 224, 100, 100);
		loginPanel.add(textLabelId);

		JTextField textFieldId = new JTextField();
		textFieldId.setBounds(255, 221, 116, 21);
		loginPanel.add(textFieldId);
		textFieldId.setColumns(10);

		JLabel textLabelPwd = new JLabel("비밀번호");
		textLabelPwd.setForeground(Color.WHITE);
		textLabelPwd.setFont(new Font("나눔고딕", Font.BOLD, 24));
		textLabelPwd.setBounds(126, 337, 100, 100);
		loginPanel.add(textLabelPwd);

		JPasswordField textFieldPwd = new JPasswordField();
		textFieldPwd.setBounds(255, 334, 116, 21);
		loginPanel.add(textFieldPwd);
		textFieldPwd.setColumns(10);


		/* 로그인버튼 */
		JButton loginBtn = new JButton("로그인");

		loginBtn.setBorderPainted(false);
		loginBtn.setContentAreaFilled(false);
		loginBtn.setFocusPainted(false);
		loginBtn.setForeground(Color.WHITE);
		loginBtn.setFont(new Font("나눔고딕", Font.BOLD, 24));
		loginBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/* 구현 성공하였으나, 아이디 체크 쿼리문 작성 실패 */
				String id = textFieldId.getText().trim();
				String pwd = textFieldPwd.getText().trim();
				if(id.length() == 0 || pwd.length() == 0) {
					JOptionPane.showMessageDialog(null, "아이디 또는 비밀번호를 입력하세요 !");
				} else {
					if(mc.memberLogin(id, pwd)) {
						JOptionPane.showMessageDialog(null, "로그인 성공 ! 게임을 실행합니다.");
						PanelSwitch.changePanel(mf, loginMenu, new StoryPage(mf));
					} else {
						JOptionPane.showMessageDialog(null, "등록되지 않은 회원입니다!");
					}
				}
				
			}
		});
		loginBtn.setBounds(510, 500, 200, 100);
		loginMenu.add(loginBtn);
	}
	
}



