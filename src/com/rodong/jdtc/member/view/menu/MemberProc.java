package com.rodong.jdtc.member.view.menu;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.rodong.jdtc.member.model.dao.MemberDAO;
import com.rodong.jdtc.member.model.dto.MemberDTO;
 
public class MemberProc extends JFrame implements ActionListener {
   
   
    JPanel p;
    JTextField tfId, tfName, tfEmail;
    JPasswordField pfPwd; //비밀번호   
    JRadioButton rbUser, rbAdmin; // 유저, 관리자
    JTextArea taIntro;
    JButton btnCancel, btnUpdate,btnDelete; //가입, 취소, 수정 , 탈퇴 버튼
   
    GridBagLayout gb;
    GridBagConstraints gbc;
    Member_List mList ;
   
    public MemberProc(){ //가입용 생성자
       
        createUI(); // UI작성해주는 메소드
        btnUpdate.setEnabled(true);
        btnUpdate.setVisible(true);
        btnDelete.setEnabled(true);
        btnDelete.setVisible(true);
       
       
    }//생성자
   
    public MemberProc(Member_List mList){ //가입용 생성자
       
        createUI(); // UI작성해주는 메소드
        btnUpdate.setEnabled(true);
        btnUpdate.setVisible(true);
        btnDelete.setEnabled(true);
        btnDelete.setVisible(true);
        this.mList = mList;
       
    }//생성자

 
       
    //MemberDTO 의 회원 정보를 가지고 화면에 셋팅해주는 메소드
    private void viewData(MemberDTO vMem){
       
		int no = vMem.getNo();
		String name = vMem.getName();
		String email = vMem.getEmail();
		String id = vMem.getId();
		String pwd = vMem.getPwd();
		String type = vMem.getType();
		int life = vMem.getLife();
		java.util.Date saveDate = vMem.getSaveDate();
		int floor =  vMem.getFloor();
		int saveNo = vMem.getSaveNo();
		String ny = vMem.getNy();
		java.util.Date signupDate = vMem.getSignupDate();
       
        //화면에 세팅
        tfId.setText(id);
        tfId.setEditable(false); //편집 안되게
        pfPwd.setText(""); //비밀번호는 안보여준다.
        tfName.setText(name);
        tfEmail.setText(email);
       
       
        if(type.equals("User")){
            rbUser.setSelected(true);
        }else if(type.equals("Admin")){
        	rbAdmin.setSelected(true);
        }
       
        tfEmail.setText(email);
        taIntro.setText("회원관리");
   
       
    }//viewData
   
   
   
    private void createUI(){
        this.setTitle("회원정보");
        gb = new GridBagLayout();
        setLayout(gb);
        gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;

        
        //이름
        JLabel bName = new JLabel("이름 :");
        tfName = new JTextField(20);
        gbAdd(bName,0,0,1,1);
        gbAdd(tfName,1,0,1,1);
       
        //이메일
        JLabel bEmail = new JLabel("이메일: ");
        tfEmail = new JTextField(40);
        gbAdd(bEmail, 0,1,1,1);
        gbAdd(tfEmail, 1, 1, 1,1);
        
        //아이디
        JLabel bId = new JLabel("아이디 : ");
        tfId = new JTextField(20);     
        gbAdd(bId, 0, 2, 1, 1);
        gbAdd(tfId, 1, 2, 1, 1);
       
        //비밀번호
        JLabel bPwd = new JLabel("비밀번호 : ");
        pfPwd = new JPasswordField(20);
        gbAdd(bPwd, 0, 3, 1, 1);
        gbAdd(pfPwd, 1, 3, 1, 1);
       
        //타입
        JLabel bGender = new JLabel("구분 : ");
        JPanel pGender = new JPanel(new FlowLayout(FlowLayout.LEFT));
        rbUser = new JRadioButton("유저",true);
        rbAdmin = new JRadioButton("관리자",true);
        ButtonGroup group = new ButtonGroup();
        group.add(rbUser);
        group.add(rbAdmin);
        pGender.add(rbUser);
        pGender.add(rbAdmin);      
        gbAdd(bGender, 0,4,1,1);
        gbAdd(pGender,1,4,1,1);
       

       
        //버튼
        JPanel pButton = new JPanel();
        btnUpdate = new JButton("정보 수정"); 
        btnDelete = new JButton("회원 탈퇴");
        btnCancel = new JButton("취소");     
        pButton.add(btnUpdate);
        pButton.add(btnDelete);
        pButton.add(btnCancel);    
        gbAdd(pButton, 0, 10, 4, 1);
       
        //버튼에 이벤트
        btnUpdate.addActionListener(this);
        btnCancel.addActionListener(this);
        btnDelete.addActionListener(this);
       
        setSize(400,250);
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE); //dispose(); //현재창만 닫는다.
       
       
    }
   
    //그리드백레이아웃에 붙이는 메소드
    private void gbAdd(JComponent c, int x, int y, int w, int h){
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.gridwidth = w;
        gbc.gridheight = h;
        gb.setConstraints(c, gbc);
        gbc.insets = new Insets(2, 2, 2, 2);
        add(c, gbc);
    }
   
    public static void main(String[] args) {
       
        new MemberProc();
    }
   
 
    @Override
    public void actionPerformed(ActionEvent ae) {
      if(ae.getSource() == btnCancel){
            this.dispose(); //창닫기 (현재창만 닫힘)
    
        }else if(ae.getSource() == btnUpdate){
            UpdateMember();            
        }else if(ae.getSource() == btnDelete){
            int x = JOptionPane.showConfirmDialog(this,"정말 삭제하시겠습니까?","삭제",JOptionPane.YES_NO_OPTION);
           
            if (x == JOptionPane.OK_OPTION){
                deleteMember();
            }else{
                JOptionPane.showMessageDialog(this, "삭제를 취소하였습니다.");
            }
        }
       
        //jTable내용 갱신 메소드 호출
//        mList.jTableRefresh();
       
    }//actionPerformed 
   
   
    private void deleteMember() {
        String id = tfId.getText();
        String pwd = pfPwd.getText();
        if(pwd.length()==0){ //길이가 0이면
           
            JOptionPane.showMessageDialog(this, "비밀번호를 꼭 입력하세요!");
            return; //메소드 끝
        }
        //System.out.println(mList);
        MemberDAO dao = new MemberDAO();
//        boolean ok = dao.deleteMember(id, pwd);
       
        if(ok){
            JOptionPane.showMessageDialog(this, "삭제완료");
            dispose();         
           
        }else{
            JOptionPane.showMessageDialog(this, "삭제실패");
           
        }          
       
    }//deleteMember
   
    private void UpdateMember() {
       
        //1. 화면의 정보를 얻는다.
        MemberDTO dto = getViewData();     
        //2. 그정보로 DB를 수정
        MemberDAO dao = new MemberDAO();
//        boolean ok = dao.updateMember(dto);
       
        if(ok){
            JOptionPane.showMessageDialog(this, "수정되었습니다.");
            this.dispose();
        }else{
            JOptionPane.showMessageDialog(this, "수정실패: 값을 확인하세요");   
        }
    }
 
   
    public MemberDTO getViewData(){
       
        //화면에서 사용자가 입력한 내용을 얻는다.
        MemberDTO dto = new MemberDTO();
        String name =tfName.getText();
        String id = tfId.getText();
        String pwd = pfPwd.getText();
        String addr = tfEmail.getText();
        //String birth = birth1+"/"+birth2+"/"+birth3;
        String type= "";
        if(rbUser.isSelected()){
            type = "User";
        }else if(rbAdmin.isSelected()){
            type = "Admin";
        }
       
        String email = tfEmail.getText();
        String intro = taIntro.getText();
       
        //dto에 담는다.
        dto.setName(name);
        dto.setId(id);
        dto.setPwd(pwd);
        dto.setEmail(email);
        dto.setType(type);
       
        return dto;
    }
   
}//end