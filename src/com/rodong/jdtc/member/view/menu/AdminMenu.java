package com.rodong.jdtc.member.view.menu;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.rodong.jdtc.member.view.implement.ImgAttachPanel;
import com.rodong.jdtc.member.view.implement.MainFrame_Test;

public class AdminMenu extends JPanel {

	private JFrame mf;
	private JPanel panel;
	private JPanel UserManage;
	//	private JPanel selectyById;
	//	private JPanel UserManage;

	public AdminMenu(MainFrame_Test mf) {

		this.mf = mf;
		this.setLayout(null);
		ImgAttachPanel gp = null;

		panel = this;

		JLabel label = new JLabel(new ImageIcon(new ImageIcon("img/Menu/AdminBackground01.png").getImage().getScaledInstance(1385, 760, 0)));
		label.setBounds(-10, -20, 1400, 800); 

		/* 1. 회원 관리창 버튼 - 팝업 */
		JLabel button1 = new JLabel(new ImageIcon(new ImageIcon("img/button/AdminMenuButton.png").getImage().getScaledInstance(300, 100, 0)));
		button1.setBounds(500, 170, 300, 150);
		JLabel text1 = new JLabel("회원 관리");
		text1.setBounds(580, 195, 200, 100);
		text1.setFont(new Font("굴림체", Font.BOLD, 15));
		text1.setForeground(Color.white);
		text1.setFont(text1.getFont().deriveFont(30.0f));


		/* 2. 퀴즈 관리창 버튼 - 팝업 */
		JLabel button2 = new JLabel(new ImageIcon(new ImageIcon("img/button/AdminMenuButton.png").getImage().getScaledInstance(300, 100, 0)));
		button2.setBounds(500, 290, 300, 150);
		JLabel text2 = new JLabel("퀴즈 관리");
		text2.setBounds(580, 282, 200, 170);
		text2.setFont(new Font("굴림체", Font.BOLD, 15));
		text2.setForeground(Color.white);
		text2.setFont(text2.getFont().deriveFont(30.0f));

		/* 관리자 모드 종료 버튼 */
		JLabel button3 = new JLabel(new ImageIcon(new ImageIcon("img/button/AdminMenuButton.png").getImage().getScaledInstance(300, 100, 0)));
		button3.setBounds(500, 410, 300, 150);
		JLabel text3 = new JLabel("관리자모드 종료");
		text3.setBounds(570, 366, 200, 240);
		text3.setFont(new Font("굴림체", Font.BOLD, 15));
		text3.setForeground(Color.white);
		text3.setFont(text3.getFont().deriveFont(22.0f));

		JLabel button4 = new JLabel(new ImageIcon(new ImageIcon("img/button/AdminButton.gif").getImage().getScaledInstance(220, 80, 0)));
		button4.setBounds(22, 85, 400, 100);



		/* 1. 회원 관리창 버튼 입력시 나오는 화면*/
		button1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new Member_List();				
//				/* 회원 관리 팝업창의 기본 프레임*/
//				JFrame popUpFrame = new JFrame("회원 관리");										
//				popUpFrame.setSize( 900, 600);
//				popUpFrame.setLocationRelativeTo(null);
//				
//				/* 회원 관리 팝업창의 초기 버튼을 올릴 판넬*/
//				JPanel UserManage = new JPanel();																
//				UserManage.setLayout(null);
				UserManage.setSize(900, 600);
				
//				/* 멤버 리스트를 띄울 판넬 */
//				JPanel Jno = new JPanel(new GridLayout(1,1));
////				JPanel
				
				
				/* 1-1. 회원 정보 조회 버튼 */
				JButton SelectUser = new JButton("정보 수정 및 삭제");								
				SelectUser.setFont(SelectUser.getFont().deriveFont(15.0f));
				SelectUser.setLayout(null);
				SelectUser.setBounds(350, 10, 200, 40);

//				/* 1-1. 회원 정보 조회 버튼 입력시 나오는 화면 */
//				SelectUser.addMouseListener(new MouseAdapter() {
//					@Override	
//					public void mouseClicked(MouseEvent e) {
//						/* 1. 회원 정보 조회 내용을 올릴 판넬 */
//						JPanel SelectUserList = new JPanel();												
//						SelectUserList.setLayout(null);
//						SelectUserList.setSize(900, 600);
//						
//						/* 텍스트 출력 */
//						JTextField SelectUserTitle = new JTextField("원하시는 조회방법을 선택하세요");
//						SelectUserTitle.setBounds(310, 50, 240, 40);
//						SelectUserTitle.setBackground(Color.LIGHT_GRAY);
//						SelectUserTitle.setHorizontalAlignment(JTextField.CENTER);
//						SelectUserTitle.setBorder(null);
//						SelectUserTitle.setFont(SelectUserTitle.getFont().deriveFont(15.0f));
//						SelectUserTitle.setFont(new Font("바탕체", Font.BOLD, 15));
//						
//						/* 1-1-1. 아이디로 조회 버튼 */
//						JButton SelectById = new JButton("아이디로 조회");								
//						SelectById.setFont(SelectUser.getFont().deriveFont(15.0f));
//						SelectById.setLayout(null);
//						SelectById.setBounds(350, 150, 150, 40);
//						
//						/* 1-1-1. 아이디로 조회 버튼 클릭시 나오는 팝업  - 여기서 입력받은 내용으로 DB내의 회원정보 출력 */
//						SelectById.addMouseListener(new MouseAdapter() {
//							@Override
//							public void mouseClicked(MouseEvent e) {
//
//								String SelectId = JOptionPane.showInputDialog("아이디를 입력하세요");
//								System.out.println(SelectId);
//							}
//						});
//						
//						/* 1-1-2. 이름으로 조회버튼  */
//						JButton SelectByName = new JButton("이름으로 조회");								
//						SelectByName.setFont(SelectUser.getFont().deriveFont(15.0f));
//						SelectByName.setLayout(null);
//						SelectByName.setBounds(350, 250, 150, 40);
//						
//						/* 1-1-2. 이름으로 조회버튼 클릭시 나오는 팝업 - 여기서 입력받은 내용으로 DB내의 회원 정보 출력 */
//						SelectByName.addMouseListener(new MouseAdapter() {
//							@Override
//							public void mouseClicked(MouseEvent e) {
//
//								String SelectName = JOptionPane.showInputDialog("이름을 입력하세요");
//								System.out.println(SelectName);
//							}
//						});
//						
//						/* 1-1-3. 이메일로 조회버튼 */
//						JButton SelectByEmail = new JButton("이메일로 조회");								
//						SelectByEmail.setFont(SelectUser.getFont().deriveFont(15.0f));
//						SelectByEmail.setLayout(null);
//						SelectByEmail.setBounds(350, 350, 150, 40);
//						
//						/* 1-1-3.  이메일로 조회버튼 클릭시 나오는 팝업 - 여기서 입력받은 내용으로 DB내의 회원 정보 출력 */
//						SelectByEmail.addMouseListener(new MouseAdapter() {
//							@Override
//							public void mouseClicked(MouseEvent e) {
//
//								String SelectEmail = JOptionPane.showInputDialog("이메일을 입력하세요");
//								System.out.println(SelectEmail);
//							}
//						});
//
//						SelectUserList.add(SelectUserTitle);
//						SelectUserList.add(SelectById);
//						SelectUserList.add(SelectByName);
//						SelectUserList.add(SelectByEmail);
//
//						UserManage.setVisible(false);
//						SelectUserList.setVisible(true);
//						popUpFrame.add(SelectUserList);
//					}
//				});
//
//				/* 1-2. 신규회원 등록 버튼 */
//				JButton SelectQuiz = new JButton("신규 회원 등록");								
//				SelectQuiz.setFont(SelectUser.getFont().deriveFont(15.0f));
//				SelectQuiz.setLayout(null);
//				SelectQuiz.setBounds(350, 150, 150, 40);
//
//				SelectUser.addMouseListener(new MouseAdapter() {
//					@Override
//					public void mouseClicked(MouseEvent e) {
//						JPanel SelectQuizList = new JPanel();												
//						SelectQuizList.setLayout(null);
//						SelectQuizList.setBounds(450, 450, 300, 150);
//
//						JButton SelectAllQuiz = new JButton("모든 퀴즈 보기");
//
//					}
//				});
//
//
//

				UserManage.add(SelectUser);
////				UserManage.add(SelectQuiz);
//				popUpFrame.add(UserManage);
//				popUpFrame.setVisible(true);

			}
		});

		button2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

			}
		});

		button3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

			}
		});




		this.add(label);
		this.add(button1);
		this.add(button2);
		this.add(button3);

		this.setComponentZOrder(button1, 1);
		this.setComponentZOrder(text1, 0);
		this.setComponentZOrder(label, 2);
		this.setComponentZOrder(button2, 1);
		this.setComponentZOrder(text2, 0);
		this.setComponentZOrder(button3, 1);
		this.setComponentZOrder(text3, 0);
	}
}


