package com.rodong.jdtc.member.view.sublist;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.rodong.jdtc.member.model.service.MemberService;
import com.rodong.jdtc.member.view.implement.MainFrame_Test;
import com.rodong.jdtc.member.view.implement.PanelSwitch;
import com.rodong.jdtc.member.view.menu.LoginMenu;

/* StartPage : 프로그램 실행 시 최초로 출력되는 페이지 */
public class StartPage extends JPanel {

	private MainFrame_Test mf;
	
	/*시작화면 패널 생성 */
	public StartPage(MainFrame_Test mf) {
		
		/* 이미지 출력 실패 */
		Image bgimg = new ImageIcon(
				"img/Menu/startBeforeLogin.png").getImage().getScaledInstance(1385, 760, 0);
		JLabel startBgImg = new JLabel(new ImageIcon(bgimg));
		
		this.setSize(1400, 800);
		this.setLayout(null);
		this.setBorder(null);
		mf.getContentPane().add(this);
		this.add(startBgImg);
		
		
		/* "아무 곳이나 누르세요!" 출력 창 */
		JLabel pressAnywhere = new JLabel("아무 곳이나 누르세요!");
		pressAnywhere.setHorizontalAlignment(SwingConstants.CENTER);
		pressAnywhere.setForeground(Color.WHITE);
		pressAnywhere.setFont(new Font("나눔고딕 ExtraBold", Font.BOLD, 46));
		pressAnywhere.setLocation(360, 600);
		pressAnywhere.setSize(677,111);
		

		this.add(pressAnywhere);

		JPanel thisPanel = this;

		thisPanel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				PanelSwitch.changePanel(mf, thisPanel, new LoginMenu(mf));
			}
		});



	}
}
