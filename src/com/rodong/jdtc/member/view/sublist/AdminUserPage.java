package com.rodong.jdtc.member.view.sublist;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class AdminUserPage implements ActionListener {

	JFrame jframe;
	JButton ModifyB, DeleteB, ExitB;
	JTextArea jtext;
	JTextField NoT, NameT, EmailT, IdT, PwdT,DateT;
	ArrayList list = new ArrayList();
	
	AdminUserPage(){
		jframe = new JFrame();
		ModifyB = new JButton("회원정보 변경");
		DeleteB = new JButton("회원정보 삭제");
		ExitB = new JButton("종료");
		jtext = new JTextArea();
		NoT = new JTextField(5);
		NameT = new JTextField(10);
		EmailT = new JTextField(20);
		IdT = new JTextField(20);
		PwdT = new JTextField(20);
		DateT = new JTextField(30);
		
	}
	
	void addLayout() {
		jframe.setLayout(new BorderLayout());
		jframe.add(jtext, BorderLayout.CENTER);
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(1, 3));
		panel.add(ModifyB);
		panel.add(DeleteB);
		panel.add(ExitB);
		jframe.add(panel, BorderLayout.SOUTH);
		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(6, 2));
		p1.add(new JLabel("회원 정보"));
		p1.add(NoT);
		p1.add(new JLabel("이름"));
		p1.add(NameT);
		p1.add(new JLabel("이메일"));
		p1.add(EmailT);
		p1.add(new JLabel("아이디"));
		p1.add(IdT);
		p1.add(new JLabel("비밀번호"));
		p1.add(PwdT);
		p1.add(new JLabel("가입 날짜"));
		p1.add(DateT);
		jframe.add(p1, BorderLayout.WEST);
		jframe.setSize(700, 400);
		jframe.setVisible(true);
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	void eventProc() {
		ModifyB.addActionListener(this);
		DeleteB.addActionListener(this);
		ExitB.addActionListener(this);
		
//		NoT.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
////				int No = NoT.
//				
//			}
//		});
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
