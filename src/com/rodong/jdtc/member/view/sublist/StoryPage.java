package com.rodong.jdtc.member.view.sublist;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.rodong.jdtc.member.view.implement.MainFrame_Test;
import com.rodong.jdtc.member.view.implement.PanelSwitch;

public class StoryPage extends JPanel{

	/* 패널 스위치으로 구현한 스토리페이지 첫 화면 */
	public StoryPage(MainFrame_Test mf) {

		/* 액션 리스너에 사용하기 위해 this로 패널 선언 */
		JPanel thisPanel = this;
		this.setSize(1400, 800);

		/* 배경이미지 JLabel 삽입 */
		Image backgroundImage = new ImageIcon("img/story/story1.PNG").getImage().getScaledInstance(1385, 760, 0);
		JLabel bg = new JLabel(new ImageIcon(backgroundImage));

		/* text라벨 생성 및 프레임에 삽입 */
		JLabel text1 = new JLabel();
		text1.setForeground(Color.white);
		text1.setBounds(10, 660, 200, 100);
		text1.setFont(new Font("굴림체", Font.BOLD, 40));
		text1.setText("skip");
		text1.setHorizontalAlignment(JLabel.CENTER); // 수평 가운데 정렬
		mf.getContentPane().add(text1);

		this.add(bg);

		/* 패널 클릭 시 이동하는 액션리스너 생성 */
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				/* 프레임에 올린 text1라벨 제거 및 changePanel을 통해 Panel 체인지 */
				mf.remove(text1);
				thisPanel.setVisible(false);
				PanelSwitch.changePanel(mf, thisPanel, new StoryPage2(mf));
			}
		});


		/* text1(skip버튼)라벨을 누를 시 바로 단계선택창으로 이동하는 액션리스너 생성 */
		text1.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				mf.remove(text1);
				thisPanel.setVisible(false);
				PanelSwitch.changePanel(mf, thisPanel, new SelectLevelPage(mf));
			}
		});
	}

}

/* StoryPage2 외부클래스로 생성(위와 동일함) */
class StoryPage2 extends JPanel{ 

	public StoryPage2(MainFrame_Test mf) {

		JPanel thisPanel2 = this;
		Image backgroundImage = new ImageIcon("img/story/story2.PNG").getImage().getScaledInstance(1385, 760, 0);
		JLabel bg = new JLabel(new ImageIcon(backgroundImage));

		this.add(bg);
		
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				thisPanel2.setVisible(false);
				PanelSwitch.changePanel(mf, thisPanel2, new StoryPage3(mf));
			}
		});

	}
}

/* StoryPage3 외부클래스로 생성(위와 동일함) */
class StoryPage3 extends JPanel{ 

	public StoryPage3(MainFrame_Test mf) {

		JPanel thisPanel3 = this;
		Image backgroundImage = new ImageIcon("img/story/story3.PNG").getImage().getScaledInstance(1385, 760, 0);
		JLabel bg = new JLabel(new ImageIcon(backgroundImage));

		this.add(bg);

		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				thisPanel3.setVisible(false);
				PanelSwitch.changePanel(mf, thisPanel3, new StoryPage4(mf));

			}
		});

	}
}

/* StoryPage4 외부클래스로 생성(위와 동일함) */
class StoryPage4 extends JPanel{ 
	public StoryPage4(MainFrame_Test mf) {

		JPanel thisPanel4 = this;

		Image backgroundImage = new ImageIcon("img/story/story4.PNG").getImage().getScaledInstance(1385, 760, 0);
		JLabel bg = new JLabel(new ImageIcon(backgroundImage));

		this.add(bg);

		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				thisPanel4.setVisible(false);
				PanelSwitch.changePanel(mf, thisPanel4, new StoryPage5(mf));

			}
		});

	}
}

/* StoryPage5 외부클래스로 생성(위와 동일함) */
class StoryPage5 extends JPanel{ 
	public StoryPage5(MainFrame_Test mf) {

		JPanel thisPanel5 = this;

		Image backgroundImage = new ImageIcon("img/story/story5.PNG").getImage().getScaledInstance(1385, 760, 0);
		JLabel bg = new JLabel(new ImageIcon(backgroundImage));

		this.add(bg);

		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				thisPanel5.setVisible(false);
				PanelSwitch.changePanel(mf, thisPanel5, new StoryPage6(mf));
			}
		});

	}
}

/* StoryPage6 외부클래스로 생성(위와 동일함) */
class StoryPage6 extends JPanel{ 
	public StoryPage6(MainFrame_Test mf) {

		JPanel thisPanel6 = this;

		Image backgroundImage = new ImageIcon("img/story/story6.PNG").getImage().getScaledInstance(1385, 760, 0);
		JLabel bg = new JLabel(new ImageIcon(backgroundImage));

		this.add(bg);

		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				thisPanel6.setVisible(false);
				PanelSwitch.changePanel(mf, thisPanel6, new StoryPage7(mf));
			}
		});

	}
}

/* StoryPage7 외부클래스로 생성(위와 동일함) */
class StoryPage7 extends JPanel{ 
	public StoryPage7(MainFrame_Test mf) {

		JPanel thisPanel7 = this;

		Image backgroundImage = new ImageIcon("img/story/story7.PNG").getImage().getScaledInstance(1385, 760, 0);
		JLabel bg = new JLabel(new ImageIcon(backgroundImage));

		this.add(bg);

		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				thisPanel7.setVisible(false);
				PanelSwitch.changePanel(mf, thisPanel7, new StoryPage8(mf));
			}
		});


	}
}

/* StoryPage8 외부클래스로 생성(위와 동일함) */
class StoryPage8 extends JPanel{ 
	public StoryPage8(MainFrame_Test mf) {

		JPanel thisPanel8 = this;

		Image backgroundImage = new ImageIcon("img/story/story8.PNG").getImage().getScaledInstance(1385, 760, 0);
		JLabel bg = new JLabel(new ImageIcon(backgroundImage));

		this.add(bg);

		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				thisPanel8.setVisible(false);
				PanelSwitch.changePanel(mf, thisPanel8, new StoryPage9(mf));
			}
		});


	}
}

/* StoryPage9 외부클래스로 생성(위와 동일함) */
class StoryPage9 extends JPanel{ 
	public StoryPage9(MainFrame_Test mf) {

		JPanel thisPanel9 = this;

		Image backgroundImage = new ImageIcon("img/story/story9.PNG").getImage().getScaledInstance(1385, 760, 0);
		JLabel bg = new JLabel(new ImageIcon(backgroundImage));

		this.add(bg);

		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				thisPanel9.setVisible(false);
				PanelSwitch.changePanel(mf, thisPanel9, new StoryPage10(mf));
			}
		});


	}
}

/* StoryPage10 외부클래스로 생성(위와 동일함) */
class StoryPage10 extends JPanel{ 
	public StoryPage10(MainFrame_Test mf) {

		JPanel thisPanel10 = this;

		Image backgroundImage = new ImageIcon("img/story/story10.PNG").getImage().getScaledInstance(1385, 760, 0);
		JLabel bg = new JLabel(new ImageIcon(backgroundImage));

		this.add(bg);

		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				thisPanel10.setVisible(false);
				PanelSwitch.changePanel(mf, thisPanel10, new StoryPage11(mf));
			}
		});

	}
}

/* StoryPage11 외외부클래스로 생성(위와 동일함) */
class StoryPage11 extends JPanel{ 
	public StoryPage11(MainFrame_Test mf) {

		JPanel thisPanel11 = this;

		Image backgroundImage = new ImageIcon("img/story/story11.PNG").getImage().getScaledInstance(1385, 760, 0);
		JLabel bg = new JLabel(new ImageIcon(backgroundImage));

		this.add(bg);

		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				thisPanel11.setVisible(false);
				PanelSwitch.changePanel(mf, thisPanel11, new StoryPage12(mf));
			}
		});

	}
}

/* StoryPage12 외부클래스로 생성(위와 동일함) */
class StoryPage12 extends JPanel{ 
	public StoryPage12(MainFrame_Test mf) {

		JPanel thisPanel12 = this;

		Image backgroundImage = new ImageIcon("img/story/story12.PNG").getImage().getScaledInstance(1385, 760, 0);
		JLabel bg = new JLabel(new ImageIcon(backgroundImage));
		
		/* text라벨을 생성 */
		JLabel text1 = new JLabel();
		text1.setForeground(Color.white);
		text1.setBounds(1050, 380, 200, 100);
		text1.setFont(new Font("굴림체", Font.BOLD, 12));
		text1.setText("> 스토리 종료후 모험 떠나기");
		text1.setHorizontalAlignment(JLabel.CENTER); // 수평 가운데 정렬
		mf.getContentPane().add(text1);

		this.add(bg);

		/* text라벨 클릭 시 단계선택창으로 이동하는 액션리스너 생성 */
		text1.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				mf.remove(text1);
				thisPanel12.setVisible(false);
				PanelSwitch.changePanel(mf, thisPanel12, new SelectLevelPage(mf));
			}
		});
	}
}
