package com.rodong.jdtc.member.view.sublist;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.rodong.jdtc.member.view.implement.MainFrame_Test;
import com.rodong.jdtc.member.view.implement.PanelSwitch;
import com.rodong.jdtc.member.view.menu.MainMenu;

/* 이벤트 : Only 뒤로가기 -> "뒤로가기" button 누를 시 */

public class CharacterStatusPage extends JPanel {

	public CharacterStatusPage(MainFrame_Test mf) { 
		
		/* 액션리스너 내에서 쓸 수 있도록 패널 this로 설정 */
		JPanel thisPanel = this;

		/* JLabel에 이미지 삽입 */
		Image backgroundImage = new ImageIcon("img/Menu/ChacaterStatus.png").getImage().getScaledInstance(1385, 760, 0);
		JLabel bg = new JLabel(new ImageIcon(backgroundImage));

		/* text라벨 생성 */
		JLabel text1 = new JLabel();
		text1.setForeground(Color.white);
		text1.setBounds(100, 620, 200, 100);
		text1.setFont(new Font("굴림체", Font.BOLD, 40));
		text1.setText("뒤로 가기");
		text1.setHorizontalAlignment(JLabel.CENTER); // 수평 가운데 정렬
		mf.getContentPane().add(text1);

		/* 현재 패널에 각 JLabel들 삽입 */
		bg.setLayout(null);
		this.add(bg);

		/*1. 현재까지 깬 층수(db연동 필요) */
		/* id, pwd 입력해서 selectMember메소드로 찾은 다음
		 * 그 유저의 세이브데이터 불러오기 -> 구현 실패 */

		/*2. 현재까지 구출한 동료 상태(db연동 필요)*/
		/* 위와 같음 -> 구현 실패 */

		/*3. 라이프 개수 (db연동 후 개수에 맞게 '하트'이미지 출력)*/
		/* 위와 같음 -> life는 int로 받아서 그 개수에 맞게 '하트'이미지 출력필요 
		 * 구현 실패 .. */

		/* 메인화면(페이지)로 돌아갈 text라벨 액션리스너 */
		text1.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				mf.remove(text1);
				thisPanel.setVisible(false);
				PanelSwitch.changePanel(mf, thisPanel, new MainMenu(mf));
			}
		});

	}
}

