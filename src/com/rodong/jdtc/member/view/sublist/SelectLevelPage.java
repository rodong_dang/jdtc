package com.rodong.jdtc.member.view.sublist;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.rodong.jdtc.member.controller.MemberController;
import com.rodong.jdtc.member.view.implement.MainFrame_Test;
import com.rodong.jdtc.member.view.implement.PanelSwitch;
import com.rodong.jdtc.member.view.menu.MainMenu;
import com.rodong.jdtc.member.view.quiz.QuizKingPage;
import com.rodong.jdtc.member.view.quiz.QuizNormalPage;
import com.rodong.jdtc.member.view.quiz.QuizSemiBossPage;

public class SelectLevelPage extends JPanel {

	MemberController memberController = new MemberController();

	/* 단계 선택 창 
	 * text 입력받아 그에 따른 단계 출력
	 * 화면 전환 : 입력한 숫자(단계)의 퀴즈 페이지로 이동
	 * */

	/* switch문 내에서 이동할 클래스의 객체를 미리 전역변수로 선언 */
	private QuizNormalPage qp = new QuizNormalPage();
	private QuizSemiBossPage qbp = new QuizSemiBossPage();
	
	public SelectLevelPage(MainFrame_Test mf) {

		/* 배경이미지를 라벨에 올리기 */
		Image backgroundImage = new ImageIcon("img/Menu/SelectFlow.PNG").getImage().getScaledInstance(1385, 760, 0); 
		JLabel bg = new JLabel(new ImageIcon(backgroundImage));

		/* text라벨 생성 */
		JLabel text1 = new JLabel();
		text1.setForeground(Color.white);
		text1.setBounds(10, 660, 200, 100);
		text1.setFont(new Font("굴림체", Font.BOLD, 40));
		text1.setText("뒤로가기");
		text1.setHorizontalAlignment(JLabel.CENTER); 
		mf.getContentPane().add(text1);

		/* JPanel생성 후  액션리스너 내에서 사용해주기 위해 this로 선언 */
		JPanel thisPanel = this;
		this.add(bg).setBounds(-10, -20, 1400, 800);


		/* 단계 입력받을 JTextField 생성 및 패널에 올려주기 */
		JTextField tf = new JTextField(30); 
		tf.setLayout(null);
		tf.setBounds(760, 379, 70, 20);
		this.add(tf);

		/* 메인메뉴로 돌아가는 액션리스너 선언 */
		text1.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				mf.remove(text1);
				thisPanel.setVisible(false);
				PanelSwitch.changePanel(mf, thisPanel, new MainMenu(mf));
			}
		});

		/* JTextField 입력 시 각 단계 퀴즈풀이창으로 이동할 수 있게 만드는 액션 리스너 선언 */
		tf.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String text = tf.getText();

				/* 스위치문을 이용해 입력받은 숫자에 따라서 각 단계로 이동하도록 함 */
				switch(text) {
				case "1": mf.setVisible(false); qp.round1VsPanel();/*1단계로 이동*/ break;
				case "2": mf.setVisible(false); qp.round1VsPanel();/*2단계로 이동*/ break;
				case "3": mf.setVisible(false); qp.round1VsPanel();/*3단계로 이동*/ break;
				case "4": mf.setVisible(false); qp.round1VsPanel();/*4단계로 이동*/ break;
				case "5": mf.setVisible(false); qbp.semiBossVsPanel1();/*5단계로 이동*/ break;
				case "6": mf.setVisible(false); qp.round2VsPanel();/*6단계로 이동*/ break;
				case "7": mf.setVisible(false); qp.round2VsPanel();/*7단계로 이동*/ break;
				case "8": mf.setVisible(false); qp.round2VsPanel();/*8단계로 이동*/ break;
				case "9": mf.setVisible(false); qp.round2VsPanel();/*9단계로 이동*/ break;
				case "10": mf.setVisible(false); qbp.semiBossVsPanel2();/*10단계로 이동*/ break;
				case "11": mf.setVisible(false); qp.round3VsPanel();/*11단계로 이동*/ break;
				case "12": mf.setVisible(false); qp.round3VsPanel();/*12단계로 이동*/ break;
				case "13": mf.setVisible(false); qp.round3VsPanel();/*13단계로 이동*/ break;
				case "14": mf.setVisible(false); qp.round3VsPanel();/*14단계로 이동*/ break;
				case "15": mf.setVisible(false); qbp.semiBossVsPanel3();/*15단계로 이동*/ break;
				case "16": mf.setVisible(false); qp.round4VsPanel();/*16단계로 이동*/ break;
				case "17": mf.setVisible(false); qp.round4VsPanel();/*17단계로 이동*/ break;
				case "18": mf.setVisible(false); qp.round4VsPanel();/*18단계로 이동*/ break;
				case "19": mf.setVisible(false); qp.round4VsPanel();/*19단계로 이동*/ break;
				case "20": mf.setVisible(false); qbp.semiBossVsPanel4();/*20단계로 이동*/ break;
				case "21": mf.setVisible(false); qp.round5VsPanel();/*21단계로 이동*/ break;
				case "22": mf.setVisible(false); qp.round5VsPanel();/*22단계로 이동*/ break;
				case "23": mf.setVisible(false); qp.round5VsPanel();/*23단계로 이동*/ break;
				case "24": mf.setVisible(false); qp.round5VsPanel();/*24단계로 이동*/ break;
				case "25": mf.setVisible(false); qbp.semiBossVsPanel5();/*25단계로 이동*/ break;
				case "26": mf.setVisible(false); qp.round6VsPanel();/*26단계로 이동*/ break;
				case "27": mf.setVisible(false); qp.round6VsPanel();/*27단계로 이동*/ break;
				case "28": mf.setVisible(false); qp.round6VsPanel();/*28단계로 이동*/ break;
				case "29": mf.setVisible(false); qp.round6VsPanel();/*29단계로 이동*/ break;
				case "30": mf.setVisible(false); new QuizKingPage();/*30단계로 이동*/ break;

				default: System.out.println("text:" + text+ "잘못된 숫자입니다"); 
				}

				/* 잘못된 값을 입력해서 나오지 않을 경우 바로 쓸 수 있도록 selectAll메소드를 사용함 */
				tf.setText("");
				tf.selectAll(); 

			}
		});

		this.setLayout(null);
		JLabel jl = new JLabel("이름");

		this.add(jl);
		mf.add(this);

		mf.setVisible(true);
		mf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

